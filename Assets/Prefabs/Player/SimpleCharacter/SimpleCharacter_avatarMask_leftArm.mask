%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: SimpleCharacter_avatarMask_leftArm
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Root
    m_Weight: 1
  - m_Path: Root/Pelvis
    m_Weight: 0
  - m_Path: Root/Pelvis/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Holster_slot_back
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Neck
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Neck/Cylinder
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Neck/Head
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Neck/Head/Sphere
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Sphere
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Cylinder
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Elbow_right
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Elbow_right/Sphere
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Elbow_right/Cylinder
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Elbow_right/Sphere
      (elbow)
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Elbow_right/Wrist_right
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Elbow_right/Wrist_right/Handheld_slot_rightHand
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Elbow_right/Wrist_right/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Elbow_right/Wrist_right/Thumb_right_0
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Elbow_right/Wrist_right/Thumb_right_0/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Elbow_right/Wrist_right/Thumb_right_0/Thumb_right_1
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Elbow_right/Wrist_right/Thumb_right_0/Thumb_right_1/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Elbow_right/Wrist_right/Fingers_right_0
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Elbow_right/Wrist_right/Fingers_right_0/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Elbow_right/Wrist_right/Fingers_right_0/Fingers_right_1
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_right/Elbow_right/Wrist_right/Fingers_right_0/Fingers_right_1/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left
    m_Weight: 1
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Sphere
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Cylinder
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Elbow_left
    m_Weight: 1
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Elbow_left/Sphere
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Elbow_left/Cylinder
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Elbow_left/Sphere (elbow)
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Elbow_left/Wrist_left
    m_Weight: 1
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Elbow_left/Wrist_left/Handheld_slot_leftHand
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Elbow_left/Wrist_left/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Elbow_left/Wrist_left/Thumb_left_0
    m_Weight: 1
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Elbow_left/Wrist_left/Thumb_left_0/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Elbow_left/Wrist_left/Thumb_left_0/Thumb_left_1
    m_Weight: 1
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Elbow_left/Wrist_left/Thumb_left_0/Thumb_left_1/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Elbow_left/Wrist_left/Fingers_left_0
    m_Weight: 1
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Elbow_left/Wrist_left/Fingers_left_0/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Elbow_left/Wrist_left/Fingers_left_0/Fingers_left_1
    m_Weight: 1
  - m_Path: Root/Pelvis/Torso_bottom/Torso_top/Shoulder_left/Elbow_left/Wrist_left/Fingers_left_0/Fingers_left_1/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_right
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_right/Holster_slot_rightHip
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_right/Sphere
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_right/Cylinder
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_right/Knee_right
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_right/Knee_right/Sphere
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_right/Knee_right/Cylinder
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_right/Knee_right/Ankle_right
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_right/Knee_right/Ankle_right/Sphere
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_right/Knee_right/Ankle_right/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_right/Knee_right/Ankle_right/Toes_right
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_right/Knee_right/Ankle_right/Toes_right/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_left
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_left/Holster_slot_leftHip
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_left/Sphere
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_left/Cylinder
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_left/Knee_left
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_left/Knee_left/Sphere
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_left/Knee_left/Cylinder
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_left/Knee_left/Ankle_left
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_left/Knee_left/Ankle_left/Sphere
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_left/Knee_left/Ankle_left/Cube
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_left/Knee_left/Ankle_left/Toes_left
    m_Weight: 0
  - m_Path: Root/Pelvis/Hips_left/Knee_left/Ankle_left/Toes_left/Cube
    m_Weight: 0
