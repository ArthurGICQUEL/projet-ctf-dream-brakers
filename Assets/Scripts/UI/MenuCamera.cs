using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCamera : MonoBehaviour {
    [SerializeField] GameObject mainMenu, customMenu, bindingMenu;
    Camera cam;

    private void Awake() {
        cam = GetComponentInChildren<Camera>();
        mainMenu.SetActive(true);
        customMenu.SetActive(false);
        bindingMenu.SetActive(false);
    }

    /// <summary>
    /// Manages camera movements when user is navigating between main and custom menus
    /// </summary>
    public void RotateCam(bool toMainMenu) {
        if (toMainMenu) customMenu.SetActive(false);
        else mainMenu.SetActive(false);
        StartCoroutine(Rotation(1, toMainMenu ? 180 : -180, transform.up, () => {
            if (toMainMenu) mainMenu.SetActive(true);
            else customMenu.SetActive(true);
        }));
    }
    /// <summary>
    /// Manages camera movements when user is navigating between main and rebinding menus
    /// </summary>
    public void RotateCamToBonding(bool toMainMenu) {
        if (toMainMenu) bindingMenu.SetActive(false);
        else mainMenu.SetActive(false);
        StartCoroutine(Rotation(1, toMainMenu ? -90 : 90, transform.forward, () => {
            if (toMainMenu) mainMenu.SetActive(true);
            else bindingMenu.SetActive(true);
        }));
        StartCoroutine(Translation(1, (toMainMenu ? 1 : -1) * Vector3.right * 5));
    }
    IEnumerator Rotation(float duration, float degree, Vector3 axis, System.Action callback = null) {
        Quaternion baseRotation = transform.rotation;
        Quaternion targetRotation = Quaternion.AngleAxis(degree, axis) * baseRotation;
        for (float t = 0; t < duration; t += Time.deltaTime) {
            transform.rotation = Quaternion.Slerp(baseRotation, targetRotation, t / duration);
            yield return null;
        }
        transform.rotation = targetRotation;
        callback?.Invoke();
    }
    IEnumerator Translation(float duration, Vector3 v) {
        Vector3 basePosition = cam.transform.localPosition;
        Vector3 targetPosition = basePosition + v;
        for (float t = 0; t < duration; t += Time.deltaTime) {
            cam.transform.localPosition = Vector3.Lerp(basePosition, targetPosition, t / duration);
            yield return null;
        }
        cam.transform.localPosition = targetPosition;
    }
}
