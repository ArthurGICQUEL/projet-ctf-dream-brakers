using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    ConfigMenu _configMenu;
    [SerializeField] CharacterCreator _characterCreatorMenu;
    int colorCount;
    [SerializeField] TMPro.TMP_Text durationText;
    [SerializeField] int[] durationValues;
    int DurationIndex {
        get => _durationIndex;
        set {
            if (value < durationValues.Length && value >= 0) {
                _durationIndex = value;
                GameDatas.gameConfig.gameDuration = durationValues[_durationIndex];
                durationText.text = durationValues[value].ToString();
            }
        }
    }
    int _durationIndex;

    private void Awake() {
        DurationIndex = durationValues.Length / 2;
    }
    public void IncreaseDuration() {
        DurationIndex++;
    }
    public void DecreaseDuration() {
        DurationIndex--;
    }
    public void LoadGameScene() {
        GameDatas.playerConfigs[0].name = "Player 1";
        GameDatas.playerConfigs[1].name = "Player 2"; //Change when custom names are working
        LoadScene(1);
    }
    public void LoadScene(int scene) {
        SceneManager.LoadScene(scene);
    }
    public void ExitApp() {
        Application.Quit();
    }
}
