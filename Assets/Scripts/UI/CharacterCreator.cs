using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterCreator : MonoBehaviour {
    public string playerName {
        get => _playerName;
        set => _playerName = value;
    }

    string _playerName;

    [SerializeField] FlexibleColorPicker[] _flexibleColorPickers = new FlexibleColorPicker[2];
    [SerializeField] Image[] colorButtons1, colorButtons2;
    Image[][] allColorButtons;
    [SerializeField] Material[] materials1, materials2;
    [SerializeField] Material[][] allMaterials;
    public int[] colorModified;

    private void Awake() {
        allColorButtons = new Image[][] { colorButtons1, colorButtons2 };
        allMaterials = new Material[][] { materials1, materials2 };
        for (int i = 0; i < GameDatas.playerConfigs.Length; i++) {
            GameDatas.playerConfigs[i].materials = allMaterials[i];
        }
    }

    public void SetColorModified1(int c) {
        colorModified[0] = c;
    }
    public void SetColorModified2(int c) {
        colorModified[1] = c;
    }

    public void ChangeColors(Color color) {
        Debug.Log(color.ToString());
        for (int i = 0; i < _flexibleColorPickers.Length; i++) {
            if (colorModified[i] > -1) {
                allMaterials[i][colorModified[i]].color = allColorButtons[i][colorModified[i]].color = color;
            }
        }
    }

    public void SetPlayerConfigs(int playerIndex) {
        GameDatas.playerConfigs[playerIndex].name = playerName;
    }
}