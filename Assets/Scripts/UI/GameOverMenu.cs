using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour {
    [SerializeField] TMPro.TMP_Text playerOneName, playerTwoName, playerOneScore, playerTwoScore, playerOneWinStatus, playerTwoWinStatus;

    public void InitializeGameOverMenu(int score1, int score2, bool playerOneWins) {
        playerOneName.text = GameDatas.playerConfigs[0].name;
        playerTwoName.text = GameDatas.playerConfigs[1].name;
        playerOneScore.text = score1.ToString();
        playerTwoScore.text = score2.ToString();
        playerOneWinStatus.text = playerOneWins ? "Winner" : "Loser";
        playerTwoWinStatus.text = playerOneWins ? "Loser" : "Winner";
    }

    public void LoadScene(int scene) {
        SceneManager.LoadScene(scene);
    }
}
