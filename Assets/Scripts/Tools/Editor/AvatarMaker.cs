// Thanks to user "austintaylorx" at https://forum.unity.com/threads/how-to-create-an-avatar-mask-for-custom-gameobject-hierarchy-from-scene.574270/
// for the base of this simple yet very useful script.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AvatarMaker {
    [MenuItem("CustomTools/MakeAvatarMask")]
    private static void MakeAvatarMask() {
        GameObject activeGameObject = Selection.activeGameObject;

        if(activeGameObject != null) {
            AvatarMask avatarMask = new AvatarMask();

            avatarMask.AddTransformPath(activeGameObject.transform);

            string assetName = activeGameObject.name.Replace(':', '_').TrimEnd() + "_avatarMask";
            var path = string.Format("Assets/{0}.mask", assetName);
            AssetDatabase.CreateAsset(avatarMask, path);
        }
    }

    [MenuItem("CustomTools/MakeAvatar")]
    private static void MakeAvatar() {
        GameObject activeGameObject = Selection.activeGameObject;

        if(activeGameObject != null) {
            Avatar avatar = AvatarBuilder.BuildGenericAvatar(activeGameObject, "");
            avatar.name = activeGameObject.name;
            Debug.Log(avatar.isHuman ? "is human" : "is generic");

            string assetName = activeGameObject.name.Replace(':', '_').TrimEnd() + "_avatar";
            var path = string.Format("Assets/{0}.ht", assetName);
            AssetDatabase.CreateAsset(avatar, path);
        }
    }
}