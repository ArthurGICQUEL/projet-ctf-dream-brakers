using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boots : PassiveItem {
    [SerializeField] float duration = 0f;
    [SerializeField] float speedMultiplier = 1f;

    public override void Pickup(Player owner) {
        base.Pickup(owner);
        StartCoroutine(DoSpeedUp(owner));
    }

    IEnumerator DoSpeedUp(Player owner) {
        owner.speedMultiplier = speedMultiplier;
        yield return new WaitForSeconds(duration);
        owner.speedMultiplier = 1f;
        Break();
    }
}