using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : PassiveItem {
    [SerializeField] float health;
    [SerializeField] float totalDuration;
    [SerializeField] int nbTicks;

    public override void Pickup(Player owner) {
        base.Pickup(owner);
        StartCoroutine(DoHealing(owner)); // do the heal thing
    }

    IEnumerator DoHealing(Player owner) {
        int elapsedTicks = 0;
        float tickDuration = nbTicks != 0 ? totalDuration / nbTicks : totalDuration;
        float tickTimer = 0f, healthPerTick, amountHealed = 0f;
        bool doHeal = false;
        while(amountHealed < health) {
            yield return null;
            if(totalDuration <= 0f) {
                owner.health += health;
                Debug.Log($"Healed {health} hp");
                Break();
                yield break;
            }
            healthPerTick = 0f;
            if(nbTicks != 0) {
                tickTimer += Time.fixedDeltaTime;
                if(tickTimer >= tickDuration) {
                    tickTimer -= tickDuration;
                    healthPerTick = health / nbTicks;
                    if(elapsedTicks++ == nbTicks - 1) healthPerTick = health - amountHealed;
                    doHeal = true;
                }
            } else {
                healthPerTick = (health / totalDuration) * Time.fixedDeltaTime;
                healthPerTick = Mathf.Clamp(amountHealed + healthPerTick, 0f, health) - amountHealed;
                doHeal = true;
            }

            if(doHeal) {
                doHeal = false;
                owner.health += healthPerTick;
                amountHealed += healthPerTick;
                Debug.Log($"Healed {healthPerTick} hp");
            }
        }
        Debug.Log("Healing over");
        Break();
    }
}