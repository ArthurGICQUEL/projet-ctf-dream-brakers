using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : OffensiveItem {
    [SerializeField] protected float bulletSize = 0.05f;

    public override void Pickup(Player owner) {
        base.Pickup(owner);
    }

    protected override void OnUsed() {
        //Debug.Log($"{owner.name} tried to fire with the Pistol !");
        ShootBullet();
        Recharge();
    }

    public override void DoDamage(Player[] targets = null) {
        foreach(var target in targets) {
            target.TakeDamage(owner, damage, playerModel => {
                playerModel.AddImpulse((playerModel.center.position - itemModel.ammoExit.position).normalized * 5f);
            });
        }
    }

    bool TryShootBullet(out RaycastHit hit) {
        Vector3 origin = itemModel.transform.position;
        Vector3 dir = itemModel.ammoExit.forward * 100;
        return Physics.Raycast(origin, dir, out hit, dir.magnitude, targetsMask);
    }
    bool ShootBullet() {
        if(TryShootBullet(out RaycastHit hit)) {
            //Debug.Log($"Bullet hit {hit.collider.name}");
            if(hit.collider.gameObject.TryGetPlayer(out Player player)) {
                DoDamage(new Player[] { player });
                return true;
            }
        }
        return false;
    }

    private void Update() {
        if(owner == null) return;
        if(TryShootBullet(out RaycastHit hit)) {
            Debug.DrawLine(itemModel.ammoExit.position, hit.point, Color.green);
        } else {
            Debug.DrawRay(itemModel.ammoExit.position, itemModel.ammoExit.forward * 100, Color.red);
        }
    }
}