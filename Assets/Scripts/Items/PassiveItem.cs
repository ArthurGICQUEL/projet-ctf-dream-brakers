using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PassiveItem : Item {
    public override void Pickup(Player owner) {
        base.Pickup(owner);
        Hide();
    }
}