using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : OffensiveItem {
    public override bool canBePickedUp {
        get => !isOnGround ? base.canBePickedUp : false;
        protected set => base.canBePickedUp = value;
    }
    [SerializeField] protected float armingDelay = 3f;
    [SerializeField] protected int numberOfTickInDelay = 3;
    [SerializeField] protected float explosionDelay = 0f;
    [SerializeField] protected Collider triggerCol;
    [SerializeField] protected ParticleSystem partSysBip;

    bool isOnGround = false, isArmed = false;

    public override void Initialize(Player owner) {
        base.Initialize(owner);
        isOnGround = false;
        isArmed = false;
        triggerCol.enabled = false;
    }

    public override void Pickup(Player owner) {
        base.Pickup(owner);
    }

    protected override void OnUsed() {
        //Debug.Log($"{owner.name} tried to place a Mine !");
        // place mine on ground
        if(!isOnGround) {
            owner.RemoveItemFromToolbar(this);
            SpawnManager.instance.RecycleItem(this, false); // make item spawnable
            itemModel.SetTransformTo(AttachmentMode.None);
            transform.SetParent(null);
            transform.position = owner.transform.position;
            transform.rotation = Quaternion.identity;
            transform.localScale = Vector3.one;
            isOnGround = true;
        }
        // start arming delay (animation or not)
        StartCoroutine(StartArmSelf());
    }

    protected override void OnEndRecharging() {
        base.OnEndRecharging();
        Use();
    }

    public override void Break() {
        base.Break();
        isOnGround = false;
    }

    IEnumerator StartArmSelf() {
        if(armingDelay > 0f) {
            if(numberOfTickInDelay > 0) {
                for(int i = 0; i < numberOfTickInDelay; i++) {
                    // play "bip" animation
                    partSysBip.Play();
                    yield return new WaitForSeconds(armingDelay / numberOfTickInDelay);
                }
            }
        }
        // mine then becomes almost invisible
        ArmSelf();
        //Debug.Log($"{name} is armed !");
        yield break;
    }
    void DisarmSelf() => ArmSelf(false);
    void ArmSelf(bool arm = true) {
        itemModel.Hide();
        isArmed = arm;
        triggerCol.enabled = true;
    }

    void Explode() {
        Collider[] colliders = Physics.OverlapSphere(transform.position, range, targetsMask, QueryTriggerInteraction.Collide);
        //Debug.Log($"{name} has exploded and found {colliders.Length} colliders...");
        if(colliders.Length != 0) {
            var playerList = new List<Player>();
            Player temp;
            for(int i = 0; i < colliders.Length; i++) {
                if(colliders[i].gameObject.TryGetPlayer(out temp)) {
                    if(!playerList.Contains(temp)) playerList.Add(temp);
                }
            }
            //Debug.Log($"{name} can damage {playerList.Count} players...");
            DoDamage(playerList.ToArray());
        }
        Recharge();
    }

    public override void DoDamage(Player[] targets = null) {
        foreach(var player in targets) {
            player.TakeDamage(owner, damage, playerRagdoll => {
                playerRagdoll.AddExplosionForce(itemModel.ammoExit.position, 20, range * 2f, 10);
            }); // TODO: adjust damages based on distance from explosion source
        }
    }

    public override void Trigger(Player player, bool stay) {
        base.Trigger(player, stay);
        if(stay) {
            //Debug.Log("Mine OnTriggerEnter");
            // when mine is triggered => BOOM animation then Recharge
            if(isArmed) {
                DisarmSelf();
                Invoke(nameof(Explode), explosionDelay);
            }
        }
    }

    private void OnTriggerEnter(Collider other) {
    }
}