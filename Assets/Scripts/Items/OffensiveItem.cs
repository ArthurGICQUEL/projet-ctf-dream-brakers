using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class OffensiveItem : ActiveItem {
    public bool isMelee { get => range <= 0f; }

    public float damage = 1f;
    public float range = 0f;

    public abstract void DoDamage(Player[] targets = null);
}