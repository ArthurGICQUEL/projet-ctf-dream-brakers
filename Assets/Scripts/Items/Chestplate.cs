using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chestplate : PassiveItem {
    public override void Pickup(Player owner) {
        base.Pickup(owner);
        owner.onValidateTakeDamage = ValidateDamage;
    }

    bool ValidateDamage(Player owner, Player perp, ref float damage) {
        if(--nbUsesLeft <= 0) {
            Break();
        }
        return false;
    }

    public override void Break() {
        owner.onValidateTakeDamage = null;
        base.Break();
    }
}