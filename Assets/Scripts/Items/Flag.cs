using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flag : Item {
    public override bool isFlag { get => true; }
    public Color color { get => player.config.mainMaterial.color; }
    public Player player { get; protected set; }
    public bool isInBase = true;

    public override void Spawn(Transform spawnPoint) {
        base.Spawn(spawnPoint);
        //transform.localPosition = Vector3.zero;
    }
    public override void Initialize(Player owner) {
        base.Initialize(owner);
        if(player == null) {
            player = owner;
            itemModel.Repaint(player.config.mainMaterial);
        }
    }
    public override void Pickup(Player owner) {
        base.Pickup(owner);
    }

    public override void Drop() {
        base.Drop();
        transform.position = owner.transform.position;
    }

    public override void Break() {
        owner.RemoveItemFromToolbar(this);
        SpawnManager.instance.SpawnFlag(player.index);
        Destroy(gameObject);
        //base.Break();
    }
}