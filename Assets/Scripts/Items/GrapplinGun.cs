using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplinGun : OffensiveItem {
    [SerializeField] protected Transform hookPrefab;
    [SerializeField] protected LineRenderer lineRend;
    [SerializeField] protected float hookSpeed;
    //[SerializeField] protected float drag;
    [SerializeField] protected float closeDistance;

    Transform _hook;
    SphereCollider _hookSphere;
    //Vector3 _inertia = Vector3.zero;
    bool _hookHasBeenLaunched = false; 
    Item _stolenItem = null;

    protected override void Awake() {
        base.Awake();
        _hook = Instantiate(hookPrefab, itemModel.ammoExit);
        _hook.Reset();
        _hookSphere = _hook.GetComponentInChildren<SphereCollider>();
    }

    public override void Initialize(Player owner) {
        base.Initialize(owner);
        _hookHasBeenLaunched = false;
        _stolenItem = null;
    }

    private void Update() {
        if(lineRend != null) {
            lineRend.positionCount = 2;
            lineRend.SetPosition(0, itemModel.ammoExit.position);
            lineRend.SetPosition(1, _hook.position);
        }
    }

    public override void OnSwitchFocus(bool isFocus) {
        if(!isFocus) {
            StopAllCoroutines();
            ResetHook();
            if(!_hookHasBeenLaunched) {
                isInCooldown = false;
            }
        }
        base.OnSwitchFocus(isFocus);
    }

    protected override void OnUsed() {
        //Debug.Log($"{owner.name} tried to throw a hook with the GrapplinGun !");
        // do the grapplin shooting animation
        StartCoroutine(LaunchHook());
    }

    IEnumerator LaunchHook() {
        isInAnimation = true;
        Vector3 initialDir = _hook.forward;
        _hook.SetParent(null); // detach hook

        
        RaycastHit hit;
        float d = 0, dt = 0, dist;
        // hook goes forward
        while((dist = Vector3.Distance(itemModel.ammoExit.position, _hook.transform.position)) < range && (d += dt) < range) {
            yield return null;
            if(d > range * 0.1f) _hookHasBeenLaunched = true;
            dt = Mathf.Clamp(d + hookSpeed * Time.deltaTime, 0, range) - d;
            if(CheckForHookCollision(initialDir * dt, out hit)) {
                if(hit.collider.gameObject.TryGetPlayer(out Player player)) {
                    // steal one of the player's objects
                    _stolenItem = player.toolbar.GetRandomItem();
                    if(_stolenItem != null) {
                        GrabItem(player, _stolenItem);
                        _hookHasBeenLaunched = true;
                    }
                }
                _hook.position += initialDir * dt;
                break;
            }
            _hook.position += initialDir * dt;
        }
        // hook comes back
        Vector3 dir;
        while((dist = Vector3.Distance(itemModel.ammoExit.position, _hook.transform.position)) > 0.001) {
            yield return null;
            dir = (itemModel.ammoExit.position - _hook.transform.position);
            dt = Mathf.Clamp(hookSpeed * 2 * Time.deltaTime, 0, dist);
            if(_stolenItem == null && CheckForHookCollision(dir.normalized, out hit) && hit.collider.gameObject.TryGetPlayer(out Player player)) {
                // steal one of the player's objects (if not stolen already)
                _stolenItem = player.toolbar.GetRandomItem();
                if(_stolenItem != null) {
                    GrabItem(player, _stolenItem);
                }
            }
            _hook.position += dir.normalized * dt;
            // rotation
            if(closeDistance > 0 && dist <= closeDistance) {
                _hook.transform.rotation = Quaternion.Lerp(Quaternion.LookRotation(itemModel.ammoExit.forward, itemModel.transform.up), _hook.transform.rotation, dist / closeDistance);
            } else {
                _hook.transform.rotation = Quaternion.LookRotation(-dir);
            }
        }
        // hook is back
        _hookHasBeenLaunched = false;
        ResetHook();
        isInAnimation = false;
        if(_stolenItem != null) {
            //owner.RemoveItemFromToolbar(this);
            Player lastOwner = owner;
            Break();
            lastOwner.TryPickupItem(_stolenItem);
            _stolenItem = null;
        } else {
            Recharge();
        }
    }

    void GrabItem(Player itemOwner, Item item) {
        itemOwner.RemoveItemFromToolbar(_stolenItem);
        item.AttachTo(_hook, AttachmentMode.Center);
        item.ResetTransform();
    }

    void ResetHook() {
        _hook.SetParent(itemModel.ammoExit);
        _hook.Reset();
    }

    bool CheckForHookCollision(Vector3 dir, out RaycastHit hit) {
        return Physics.SphereCast(_hook.position, _hookSphere.radius, dir, out hit, dir.magnitude, targetsMask);
    }

    public override void DoDamage(Player[] targets = null) {
        //throw new System.NotImplementedException();
    }
}