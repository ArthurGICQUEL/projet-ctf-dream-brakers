using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;

public class Knife : OffensiveItem {

    public override void Pickup(Player owner) {
        base.Pickup(owner);
    }

    protected override void OnUsed() {
        //Debug.Log($"{owner.name} did a slash with the Knife !");
        owner.PlayAnimationTrigger(useAnimation);
        Recharge(true);
    }

    public override void DoDamage(Player[] targets = null) {
        //Debug.Log($"{owner} deals many damage ! ({damage})");
        if(targets != null) {
            foreach(var target in targets) {
                target.TakeDamage(owner, damage);
            }
        }
    }
    public override void OnEndUseAnimation() {
        base.OnEndUseAnimation();
        CheckForBreak();
    }
}