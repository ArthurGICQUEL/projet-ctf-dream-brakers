using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagModel : ItemModel {
    [SerializeField] MeshRenderer glowObject;
    [SerializeField] TrailRenderer trailObject;

    public override void Repaint(Material mat, RepaintPredicate predicate = null) {
        glowObject.material = mat;
        trailObject.material = mat;
    }
}