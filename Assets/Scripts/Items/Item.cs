using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour {
    public Player owner { get; set; }
    public virtual bool canBeSwitched {
        get => true;
    }
    public virtual bool canBePickedUp { 
        get => !_hasJustDropped && col.enabled; 
        protected set {
            col.enabled = value;
        }
    }
    public virtual bool isFlag { get => false; }
    public Transform activeAttachment { get => itemModel?.handHeldTrans; }
    public Transform inactiveAttachment { get => itemModel?.holsteredTrans; }
    public Transform ammoExitTrans { get => itemModel?.ammoExit; }
    public bool canBeDropped { get => !(this is Flag); }

    [HideInInspector] public Item prefab;

    public bool canBeStored = true;
    public Sprite sprite = null;
    [SerializeField] ItemModel _modelPrefab = null;
    public BodyPart activeAttachmentPart = BodyPart.None;
    public HoldingMode activeHoldingMode = HoldingMode.None;
    public BodyPart inactiveAttachmentPart = BodyPart.None;
    [SerializeField] protected float dropPickableDelay;
    [SerializeField] protected int nbUsesMax = 1;

    protected int nbUsesLeft;
    protected Transform modelParent;
    protected ItemModel itemModel = null; 
    protected GameObject haloEffect = null;

    protected Collider col = null;

    bool _hasJustDropped = false;

    protected virtual void Awake() {
        modelParent = transform.GetChild(0);
        if(_modelPrefab != null) {
            itemModel = Instantiate(_modelPrefab, modelParent.position, modelParent.rotation, modelParent);
            col = itemModel.GetComponentInChildren<Collider>();
        }
        canBePickedUp = false;
        haloEffect = GetComponentsInChildren<Transform>().FirstOrDefault(t => t.name.Equals("Quad_Halo"))?.gameObject; // Boouuuh, dirty dirty, but me so lazy -_-
        HideHalo();

        owner = null;
    }

    public virtual void Initialize(Player owner) {
        this.owner = owner;
        nbUsesLeft = nbUsesMax;
    }
    public virtual void Pickup(Player owner) {
        //Debug.Log($"{this} was picked up !!");
        Initialize(owner);
        canBePickedUp = false;
        SpawnManager.instance.NotifySpawnerForPickup(this);
        HideHalo();
    }
    public virtual void Spawn(Transform spawnPoint) {
        transform.SetParent(spawnPoint);
        // make item visible and reachable
        Show();
        Activate();
        canBePickedUp = true;
        // and appear as though the model hovers in the air with possibly some cool effects
        itemModel.Center();
        ShowHalo();
        ResetTransform();
        transform.localPosition = Vector3.up;
    }

    public virtual void OnSwitchFocus(bool isFocus) {
    }
    public virtual void Break() {
        owner.RemoveItemFromToolbar(this);
        SpawnManager.instance.RecycleItem(this, true); // repurpose item through SpawnManager
        //Debug.Log($"{name} broke. :/");
        //Destroy(gameObject);
    }
    public virtual void Drop() {
        owner.RemoveItemFromToolbar(this);

        itemModel.SetTransformTo(AttachmentMode.None);
        transform.SetParent(null);
        ResetTransform();
        transform.position = owner.transform.position + Vector3.up;

        _hasJustDropped = true;
        canBePickedUp = true;
        Invoke(nameof(HasDroppedForLongEnough), dropPickableDelay);
    }

    public virtual void Trigger(Player player, bool stay) {
    }

    void HasDroppedForLongEnough() {
        _hasJustDropped = false;
    }

    public void AttachTo(Transform parent, AttachmentMode attachmentMode) {
        transform.SetParent(parent ?? owner.transform);
        itemModel.SetTransformTo(attachmentMode);
        Show(parent != null); // show only if a parent transform has been specified
    }
    public void ResetTransform() {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
    }

    public Transform GetRaycastTransform() {
        return itemModel.ammoExit;
    }
    public void HideHalo() => ShowHalo(false);
    public virtual void ShowHalo(bool show = true) {
        haloEffect?.SetActive(show);
    }

    public void Hide() => Show(false);
    public virtual void Show(bool show = true) {
        modelParent.gameObject.SetActive(show);
    }

    public void Deactivate() => Activate(false);
    public virtual void Activate(bool activate = true) {
        gameObject.SetActive(activate);
    }

    public override string ToString() {
        return $"(Item: \"{name}\")";
    }

    public enum AttachmentMode {
        None, Center, Holstered, HandHeld
    }
    public enum HoldingMode {
        None, Plate, Gun, Shotgun
    }
}