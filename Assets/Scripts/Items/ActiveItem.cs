using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class ActiveItem : Item {
    public override bool canBeSwitched {
        get => !isInAnimation;
    }

    [SerializeField] protected float cooldown = 1f;
    [SerializeField] protected PlayerAnim useAnimation = PlayerAnim.None;
    public LayerMask targetsMask;

    protected bool isInCooldown = false;
    [HideInInspector] public bool isInAnimation = false;

    public override void Initialize(Player owner) {
        base.Initialize(owner);
        isInCooldown = false;
    }

    public void Use() {
        if(!isInCooldown) {
            isInCooldown = true;
            OnUsed();
        }
    }
    protected abstract void OnUsed();
    protected virtual void OnStartRecharging() { }
    protected virtual void OnEndRecharging() { }
    protected virtual void OnCancelRecharging() { }
    public virtual void OnEndUseAnimation() { 
        isInAnimation = false;
    }

    public override void OnSwitchFocus(bool isFocus) {
        base.OnSwitchFocus(isFocus);
        if(isInCooldown) {
            if(!isFocus) {
                CancelRecharge();
            } else {
                Recharge();
            }
        }
    }
    protected bool CheckForBreak() {
        if(--nbUsesLeft <= 0) {
            Break();
            return true;
        }
        return false;
    }
    public void Recharge(bool noBreak = false) {
        //Debug.Log($"{name} started recherging...");
        if(!noBreak && CheckForBreak()) {
            return;
        }
        OnStartRecharging();
        Invoke(nameof(RechargeNow), cooldown);
    }
    private void RechargeNow() {
        isInCooldown = false;
        //Debug.Log($"{name} is now recharged !");
        OnEndRecharging();
    }
    private void CancelRecharge() {
        CancelInvoke(nameof(RechargeNow));
        OnCancelRecharging();
    }

    public void OnSwitchItem(bool switchToThis) {
        if(switchToThis) {
            if(isInCooldown) {
                Recharge();
            }
        } else {
            if(isInCooldown) {
                CancelRecharge();
            }
        }
    }
}