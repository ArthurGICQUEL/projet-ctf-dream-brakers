using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemModel : MonoBehaviour {
    public Transform center = null;
    public Transform handHeldTrans = null;
    public Transform holsteredTrans = null;
    public Transform ammoExit = null;

    public void SetTransformTo(Item.AttachmentMode mode) {
        switch(mode) {
            case Item.AttachmentMode.Center: Center(); break;
            case Item.AttachmentMode.HandHeld: Hold(); break;
            case Item.AttachmentMode.Holstered: Holster(); break;
            default: ResetTransform(); break;
        }
    }

    public void Center() {
        if(center == null) {
            ResetTransform();
            return;
        }
        transform.localPosition = -center.localPosition;
        transform.localRotation = center.localRotation;
        transform.localScale = center.localScale;
    }
    public void Hold() {
        if(handHeldTrans == null) {
            ResetTransform();
            return;
        }
        transform.localPosition = -handHeldTrans.localPosition;
        transform.localRotation = handHeldTrans.localRotation;
        transform.localScale = handHeldTrans.localScale;
    }
    public void Holster() {
        if(holsteredTrans == null) {
            ResetTransform();
            return;
        }
        transform.localPosition = -holsteredTrans.localPosition;
        transform.localRotation = holsteredTrans.localRotation;
        transform.localScale = holsteredTrans.localScale;
    }
    public void ResetTransform() {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
    }

    public delegate bool RepaintPredicate(Renderer renderer);
    Renderer[] _rends = null;
    public virtual void Repaint(Material mat, RepaintPredicate predicate = null) {
        if(_rends == null) _rends = GetComponentsInChildren<Renderer>();
        foreach(var rend in _rends) {
            if(predicate == null || predicate(rend)) rend.material = mat;
        }
    }

    public void Hide() => Show(false);
    public void Show(bool show = true) {
        for(int i = 0; i < transform.childCount; i++) {
            transform.GetChild(i).gameObject.SetActive(show);
        }
    }
}