using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour {
    public static GameManager instance = null;
    public static PlayerConfig[] playerConfigs = null;

    public bool isPaused {
        get {
            return _isPaused;
        }
        set {
            _isPaused = value;
            Time.timeScale = _isPaused ? 0 : 1;
            _pauseMenu.SetActive(value);
        }
    }
    public int numberOfPlayers { get => playerConfigs.Length; }
    public List<Player> players {
        get => _players;
        set => _players = value;
    }

    [SerializeField] float _respawnTime = 5f;
    public LayerMask blockRaycastsMask;
    public PlayerModel playerModelPrefab;
    [SerializeField] Material prefabCyan, prefabRed;
    [SerializeField] Transform[] _playerSpawns;
    [SerializeField] GameOverMenu _gameOverMenu;
    [SerializeField] GameObject _pauseMenu;

    bool _isPaused;
    bool _hasGameEnded;
    CharacterCreator _characterCreator;
    float _gameDuration = 60f;
    List<Player> _players = new List<Player>();
    PlayerInputManager _playerInputManager;

    private void Awake() {
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }

        _playerInputManager = GetComponent<PlayerInputManager>();
        SetDefaultValuesIfNecessary();
    }

    void SetDefaultValuesIfNecessary() {
        if(playerConfigs != null) return;
        List<PlayerConfig> configs = new List<PlayerConfig>();
        for(int i = 0; i < 2; i++) {
            configs.Add(new PlayerConfig($"Player {i + 1}", i == 0 ? Instantiate(prefabRed) : Instantiate(prefabCyan)));
        }
        playerConfigs = configs.ToArray();
    }
    private void Start() {
        Time.timeScale = 1;
        CreatePlayers();
        //Debug.Log(GameDatas.gameConfig.gameDuration);
        _gameDuration = GameDatas.gameConfig.gameDuration;
        StartGame();
    }
    private void CreatePlayers() {
        for(int i = 0; i < playerConfigs.Length; i++) {
            PlayerHolder playerHolder = _playerInputManager.JoinPlayer(i, i, "Keyboard " + (i + 1), Keyboard.current).GetComponentInParent<PlayerHolder>();

            playerHolder.Initialize(i);
            PlayerInput playerInput = playerHolder.player.GetComponent<PlayerInput>();

            playerHolder.player.index = i;
            players.Add(playerHolder.player);
            playerHolder.player.playerInput = playerInput;
            playerHolder.player.config = playerConfigs[i];

            playerInput.camera = playerHolder.cam;

            //playerHolder.player.transform.position = _playerSpawns[i].position;
            //playerHolder.player.transform.rotation = _playerSpawns[i].rotation;
            playerHolder.player.Spawn(_playerSpawns[i]);
        }
        // setup camera rects
        players[0].playerInput.camera.rect = new Rect(0, 0, 0.5f, 1);
        players[1].playerInput.camera.rect = new Rect(0.5f, 0, 0.5f, 1);
    }

    private void Update() {
        if (!_hasGameEnded && Input.GetKeyDown(KeyCode.Escape)) {
            TogglePause();
        }
    }
    public void StartGame() {
        _hasGameEnded = false;
        Invoke(nameof(EndGame), _gameDuration);
    }


    public void TogglePause() {
        isPaused = !isPaused;
    }
    public void EndGame() {
        _hasGameEnded = true;

        //When the game ends, the players can't move anymore but timeScale is still at 1, so animations can end
        foreach (Player player in players) {
            player.GetComponent<PlayerInput>().enabled = false;
        }

        players[0].isWinner = players[0].score > players[1].score;
        players[1].isWinner = players[0].score < players[1].score;

        _gameOverMenu.InitializeGameOverMenu(players[0].score, players[1].score, players[0].isWinner);
        _gameOverMenu.gameObject.SetActive(true);
    }
    public void OnPlayerDeath(Player player) {
        StartCoroutine(RespawnPlayer(player, _respawnTime));
    }

    IEnumerator RespawnPlayer(Player player, float delay) {
        yield return new WaitForSeconds(delay);
        player.Spawn(_playerSpawns[player.index]);
    }
}
