using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameDatas {
    public static PlayerConfig[] playerConfigs = new PlayerConfig[] {new PlayerConfig(), new PlayerConfig() };
    public static GameConfig gameConfig = GameConfig.defaultConfig;
}
public struct PlayerConfig {
    public Material mainMaterial { get => materials[0]; }
    public Material[] materials;
    public string name;

    public PlayerConfig(string name, Material mainMaterial) : this(name, new Material[] { mainMaterial }) { }
    public PlayerConfig(string name, Material[] materials) {
        this.name = name;
        this.materials = materials;
    }
}
public struct GameConfig {
    public static GameConfig defaultConfig = new GameConfig(120f);
    public float gameDuration;

    public GameConfig(float gameDuration) {
        this.gameDuration = gameDuration;
    }
}