using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnManager : MonoBehaviour {
    public static SpawnManager instance = null;
    public List<Item> spawnableItems {
        get => _spawnableItems;
        set => _spawnableItems = value;
    }

    [SerializeField] float _spawnDelay = 5f;
    [SerializeField] Item[] _spawnableItemPrefabs;
    [SerializeField] Transform[] _spawnPoints, _flagSpawnPoints;
    [SerializeField] Flag _flagPrefab;

    List<Item> _spawnableItems;
    Spawner[] _spawners;

    private void Awake() {
        if(instance == null) instance = this;
        else if(instance != this) Destroy(gameObject);
    }

    private void Start() {
        _spawnableItems = new List<Item>(_spawnableItemPrefabs);
        SetupSpawnPoints();
        for(int i = 0; i < GameManager.instance.players.Count; i++) {
            SpawnFlag(i);
        }
    }
    void SetupSpawnPoints() {
        _spawners = new Spawner[_spawnPoints.Length];
        for(int i = 0; i < _spawnPoints.Length; i++) {
            _spawners[i] = new Spawner(_spawnPoints[i]);
            SpawnRandomItem(_spawners[i]); // create 1 spawner per spawnPoint and make it spawn an item
        }
    }

    private void Update() {
        // update all spawners' time to spawn and do said spawning when time reaches zero or less
        for(int i = 0; i < _spawners.Length; i++) {
            if(_spawners[i].remainingTime > 0f) _spawners[i].remainingTime -= Time.deltaTime;
            if(_spawners[i].remainingTime <= 0f) SpawnRandomItem(_spawners[i]);
        }
    }

    void SpawnRandomItem(Spawner spawner) {
        if(_spawnableItems.Count != 0) {
            Item lastItem = spawner.item;
            if(lastItem != null) RecycleItem(lastItem, true); // if there was already an item, make it spawnable again

            // get a random index and spawn that item after making it unspawnable
            Item prefab = _spawnableItems[Random.Range(0, _spawnableItems.Count)];
            spawner.item = SpawnItem(prefab, spawner.point);
            _spawnableItems.Remove(prefab);
        }
        spawner.remainingTime = _spawnDelay; // roll back the hands of the clock of the spawn delay
    }

    public void SpawnFlag(int playerIndex) => SpawnItem(_flagPrefab, _flagSpawnPoints[playerIndex], GameManager.instance.players[playerIndex]);
    Item SpawnItem(Item prefab, Transform spawnPoint, Player owner = null) {
        Item item = Instantiate(prefab);
        item.prefab = prefab;
        if(owner != null) item.Initialize(owner);
        item.Spawn(spawnPoint);
        return item;
    }

    public void RecycleItem(Item item, bool destroy) {
        _spawnableItems.Add(item.prefab);
        if(destroy) Destroy(item.gameObject);
    }

    public void NotifySpawnerForPickup(Item item) {
        Spawner spawner = _spawners.FirstOrDefault(sp => sp.item == item);
        if(spawner != null) spawner.item = null;
    }

    [System.Serializable]
    public class Spawner {
        public Transform point;
        public Item item;
        public float remainingTime;

        public Spawner(Transform point) {
            this.point = point;
            this.item = null;
            this.remainingTime = 0f;
        }
    }
}