using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.UI.GridLayoutGroup;
using UnityEngine.InputSystem.HID;

public static class GameUtility {
    public static bool IsPlayer(this GameObject go) {
        if(go.layer == LayerMask.NameToLayer("Player")) {
            return go.GetComponentInParent<Player>() != null;
        }
        return false;
    }
    public static bool TryGetPlayer(this GameObject go, out Player player) {
        if(go.layer == LayerMask.NameToLayer("Player")) {
            player = go.GetComponentInParent<Player>();
            return player != null;
        }
        player = null;
        return false;
    }
    public static bool TryGetItem(this GameObject go, out Item item) {
        if(go.layer == LayerMask.NameToLayer("Item")) {
            item = go.GetComponentInParent<Item>();
            return item != null;
        }
        item = null;
        return false;
    }

    public static RaycastHit[] ConeCastAll(Vector3 origin, float halfAngle, Vector3 direction, float maxDistance, int layerMask) {
        List<RaycastHit> results = new List<RaycastHit>();
        float endRadius = maxDistance * Mathf.Tan(halfAngle * Mathf.Deg2Rad);
        RaycastHit[] hits = Physics.SphereCastAll(origin, endRadius, direction, maxDistance, layerMask);
        for(int i = 0; i < hits.Length; i++) {
            if(Vector3.Angle(origin, hits[i].point) <= halfAngle) {
                results.Add(hits[i]);
            }
        }
        return results.ToArray();
    }
    public static bool ConeCast(Vector3 origin, float halfAngle, Vector3 direction, out RaycastHit hit, float maxDistance, int layerMask) {
        float endRadius = maxDistance * Mathf.Tan(halfAngle * Mathf.Deg2Rad);
        RaycastHit[] hits = Physics.SphereCastAll(origin, endRadius, direction, maxDistance, layerMask);
        for(int i = 0; i < hits.Length; i++) {
            if(Vector3.Angle(origin, hits[i].point) <= halfAngle) {
                hit = hits[i];
                return true;
            }
        }
        hit = new RaycastHit();
        return false;
    }

    public static void Reset(this Transform t) {
        t.localPosition = Vector3.zero;
        t.localRotation = Quaternion.identity;
        t.localScale = Vector3.one;
    }
}