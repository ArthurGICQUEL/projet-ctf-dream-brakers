using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantKill : MonoBehaviour {
    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.TryGetPlayer(out Player player)) {
            player.health = 0;
        }
    }
}