using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModel : MonoBehaviour {
    public Player player { get; set; }
    public bool isRagdoll {
        get => _isRagdoll;
        private set {
            _isRagdoll = value;
        }
    }

    [SerializeField] bool _startRagdoll = false;
    [SerializeField] string _rightArmLayerName = "";
    [SerializeField] string _leftArmLayerName = "";

    public Transform center;
    public Transform head, torso;
    public Transform leftHand, rightHand;
    public Transform leftHip, rightHip;
    public Transform back;

    [SerializeField] Rigidbody _mainRigidbody;
    Collider[] _cols = null;
    Rigidbody[] _rbs = null;
    Animator _anim = null;
    AimIK _aimIK = null;
    bool _isRagdoll = true;
    Coroutine _rightArmFade = null, _leftArmFade = null;

    private void Awake() {
        ToggleRagdoll(_startRagdoll);
    }

    public void Initialize(bool startRagdoll, bool startVisible, Player player) {
        this.player = player;
        ToggleRagdoll(startRagdoll);
        Repaint(player.config.mainMaterial);

        _aimIK = GetComponent<AimIK>();
        if(_aimIK != null) _aimIK.defaultAimTransform = center;

        Show(startVisible);
    }

    public void ToggleRagdoll(bool ragdoll) {
        if(_cols == null) _cols = GetComponentsInChildren<Collider>(true);
        if(_rbs == null) _rbs = GetComponentsInChildren<Rigidbody>(true);
        if(_anim == null) _anim = GetComponentInChildren<Animator>(true);

        isRagdoll = ragdoll;
        if(_anim != null) _anim.enabled = !ragdoll;
        if(_rbs != null) {
            foreach (var rb in _rbs) {
                rb.isKinematic = !ragdoll;
            }
        }
        if (_cols != null) {
            foreach (var col in _cols) {
                col.enabled = ragdoll;
            }
        }
    }

    public void AddImpulse(Vector3 force) {
        if(force == Vector3.zero) return;
        _mainRigidbody.AddForce(force, ForceMode.Impulse);
    }
    public void AddExplosionForce(Vector3 position, float force, float radius, float upwardsModifyier) {
        if(force == 0f) return;
        _mainRigidbody.AddExplosionForce(force, position, radius, upwardsModifyier, ForceMode.Impulse);
    }

    public void StopAiming() {
        _aimIK.StopAiming();
    }
    public void SetAim(OffensiveItem item, float defaultAimDistance, float angleLimit) {
        _aimIK.defaultAimDistance = Mathf.Min(item.range, defaultAimDistance);
        _aimIK.angleLimit = angleLimit;
        _aimIK.StartAiming(item);
    }
    public void SetTarget(Transform target) {
        _aimIK.targetTransform = target;
    }

    public void UpdateMoveAnimation(float speedRatio) {
        _anim.SetFloat("SpeedRatio", speedRatio);
    }
    public bool PlayAnimationTrigger(PlayerAnim anim) {
        if(anim == PlayerAnim.None) return false;
        _anim.SetTrigger(anim.ToString());
        return true;
    }

    public void FadeLeftArmLayer(float fadeTime, bool fadeIn, System.Action onFadeEndsAction = null) {
        if(_leftArmFade != null) StopCoroutine(_leftArmFade); // stop fading if there is already one in action
        _leftArmFade = StartCoroutine(FadeLayer(_leftArmLayerName, fadeIn ? 1 : 0, fadeTime, onFadeEndsAction));
    }
    public void FadeRightArmLayer(float fadeTime, bool fadeIn, System.Action onFadeEndsAction = null) {
        if(_rightArmFade != null) StopCoroutine(_rightArmFade); // stop fading if there is already one in action
        _rightArmFade = StartCoroutine(FadeLayer(_rightArmLayerName, fadeIn ? 1 : 0, fadeTime, onFadeEndsAction));
    }
    IEnumerator FadeLayer(string layerName, float targetWeight, float fadeTime, System.Action onFadeEndsAction = null) {
        int layerIndex = _anim.GetLayerIndex(layerName);
        float baseWeight = _anim.GetLayerWeight(layerIndex);
        if(fadeTime > 0f && targetWeight != baseWeight) {
            for(float t = 0; t < fadeTime; t += Time.deltaTime) {
                _anim.SetLayerWeight(layerIndex, Mathf.Lerp(baseWeight, targetWeight, t / fadeTime));
                yield return null;
            }
        }
        _anim.SetLayerWeight(layerIndex, targetWeight);
        //Debug.Log($"{player}: FadeUpperBodyLayer DONE (weight is {targetWeight})");
        onFadeEndsAction?.Invoke();
        _rightArmFade = null;
        yield break;
    }

    public void OnUseAnimationEvent() {
        player.DoItemAttack();
    }
    public void OnEndUseAnimation() {
        player.OnEndUseAnimation();
    }

    public Transform GetBodyPartTransform(BodyPart bodyPart) {
        switch(bodyPart) {
            case BodyPart.RightHand: return rightHand;
            case BodyPart.LeftHand: return leftHand;
            case BodyPart.Back: return back;
            case BodyPart.RightHip: return rightHip;
            case BodyPart.LeftHip: return leftHip;
            default: return null;
        }
    }

    public void Repaint(Material mat) {
        var renderers = GetComponentsInChildren<Renderer>(true);
        for(int i = 0; i < renderers.Length; i++) {
            renderers[i].material = mat;
        }
    }

    public void Hide() => Show(false);
    public void Show(bool show = true) {
        gameObject.SetActive(show);
    }

    //public static BodyPart[] bodyParts = new BodyPart[] { BodyPart.RightHand, BodyPart.LeftHand, BodyPart.Back, BodyPart.RightHip, BodyPart.LeftHip };
    
}
public enum BodyPart {
    None, RightHand, LeftHand, Back, RightHip, LeftHip
}
public enum PlayerAnim {
    None, Attack, BecomeThePrettiestInAllTheLand
}