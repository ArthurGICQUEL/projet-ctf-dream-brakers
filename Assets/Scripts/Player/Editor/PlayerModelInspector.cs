using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerModel))]
public class PlayerModelInspector : Editor {
    GUIContent _enableRagdoll = new GUIContent("Ragdoll ON");
    GUIContent _disableRagdoll = new GUIContent("Ragdoll OFF");

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        PlayerModel model = (PlayerModel)target;

        EditorGUILayout.Space();
        if(GUILayout.Button(model.isRagdoll ? _disableRagdoll : _enableRagdoll)) {
            model.ToggleRagdoll(!model.isRagdoll);
        }
    }
}