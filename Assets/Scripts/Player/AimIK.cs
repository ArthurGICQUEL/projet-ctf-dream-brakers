using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimIK : MonoBehaviour {
    public Transform targetTransform {
        get => _targetTransform;
        set {
            _targetTransform = value;
        }
    }
    public Transform defaultAimTransform {
        get => _defaultAimTranform;
        set {
            _defaultAimTranform = value;
        }
    }
    public float defaultAimDistance {
        get => _defaultAimDistance;
        set {
            _defaultAimDistance = value;
        }
    }
    public float angleLimit {
        get => _angleLimit;
        set {
            _angleLimit = value;
        }
    }
    [SerializeField] int _iterations = 10;
    [SerializeField, Range(0, 1f)] float _weight = 1f;
    float _angleLimit = 90f;
    [SerializeField] float _minDistance = 1f;
    float _defaultAimDistance = 50f;
    [SerializeField] CharacterBone[] _spineBones;
    [SerializeField] CharacterBone[] _rightArmBones;
    [SerializeField] CharacterBone[] _leftArmBones;

    Transform _defaultAimTranform = null, _aimTransform = null, _targetTransform = null;

    public void StopAiming() => StartAiming(null);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="aimTransform">Should be the transform from which a raycast would start.</param>
    /// <param name="target">The transform of the target to aim at.</param>
    public void StartAiming(OffensiveItem item) {
        _aimTransform = item?.GetRaycastTransform();
    }

    private void LateUpdate() {
        if(_aimTransform == null || _targetTransform == null) return;

        Vector3 targetPos = GetTargetPosition();
        for(int i = 0; i < _iterations; i++) {
            for(int b = 0; b < _spineBones.Length; b++) {
                AimAtTarget(_spineBones[b].bone, targetPos, _spineBones[b].weight * _weight);
            }
        }
        //Vector3 defaultTargetPos = GetDefaultTargetPosition();
        for(int i = 0; i < _iterations; i++) {
            for(int b = 0; b < _rightArmBones.Length; b++) {
                AimAtTarget(_rightArmBones[b].bone, targetPos, _rightArmBones[b].weight * _weight);
            }
        }
    }

    IEnumerator FadeWeight(float targetWeight, float fadeTime) {
        float baseWeight = _weight;
        if(fadeTime > 0f && targetWeight != baseWeight) {
            for(float t = 0; t < fadeTime; t += Time.deltaTime) {
                _weight = Mathf.Lerp(baseWeight, targetWeight, t / fadeTime);
                yield return null;
            }
        }
        _weight = targetWeight;
        yield break;
    }

    void AimAtTarget(Transform bone, Vector3 targetPos, float weight) {
        Vector3 aimDir = _aimTransform.forward;
        Vector3 targetDir = targetPos - _aimTransform.position;
        Quaternion aimTowards = Quaternion.FromToRotation(aimDir, targetDir);
        Quaternion blendedRotation = Quaternion.Slerp(Quaternion.identity, aimTowards, weight);
        bone.rotation = blendedRotation * bone.rotation;
    }
    Vector3 GetTargetPosition() {
        Vector3 targetPos = targetTransform.position;
        Vector3 targetDir = targetPos - _aimTransform.position;
        Vector3 aimDir = _aimTransform.forward;
        float blendOut = 0f;

        float targetAngle = Vector3.Angle(targetDir, aimDir);
        if(targetAngle > _angleLimit) {
            blendOut += (targetAngle - _angleLimit) / 50f;
        }
        float targetDistance = targetDir.magnitude;
        if(targetDistance < _minDistance) {
            blendOut += _minDistance - targetDistance;
        }
        //Debug.Log($"deltaAngle/50 = {(targetAngle - _angleLimit)/20f}; deltaDist = {_minDistance - targetDistance}; blendOut = {blendOut};");
        Vector3 dir = Vector3.Slerp(targetDir, aimDir, blendOut);
        return _aimTransform.position + dir;
    }
    Vector3 GetDefaultTargetPosition() {
        return defaultAimTransform.position + defaultAimTransform.forward * defaultAimDistance;
    }
}
[System.Serializable]
public class CharacterBone {
    public Transform bone;
    public float weight;
}