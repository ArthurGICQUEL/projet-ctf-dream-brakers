using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.HID;
using UnityEngine.InputSystem.XR;

public class Player : MonoBehaviour {
    public bool isDead { get => _isDead; private set => _isDead = value; }
    public float health {
        get => _health;
        set {
            _health = Mathf.Clamp(value, 0f, _maxHealth);
            healthBar.statValue = _health;
            if (_health == 0) Die();
        }
    }
    public float speed { get => _moveSpeed * speedMultiplier; }
    public float groundAcceleration { get => _groundAcceleration * speed; }
    public float inAirAcceleration { get => _airAcceleration * speed; }
    public float angularAcceleration { get => _turnAcceleration * _turnSpeed; }
    public int score {
        get => _score;
        set {
            _score = value;
        }
    }
    public Toolbar toolbar {
        get => _toolbar;
        set {
            _toolbar = value;
            _toolbar.owner = this;
        }
    }
    public StatBar healthBar {
        get => _healthBar;
        set {
            _healthBar = value;
            _healthBar.Setup(0f, _maxHealth);
        }
    }
    public PlayerConfig config {
        get => _config;
        set {
            _config = value;
            ApplyConfig(value);
        }
    }
    public PlayerModel playerModel {
        get => _playerModel;
        set {
            _playerModel = value;
        }
    }

    [SerializeField, Tooltip("Speed in unit/sec. (i.e. The distance reached in one second)")] 
    float _moveSpeed = 5f;
    [SerializeField, Tooltip("Acceleration in [moveSpeed]/sec. (i.e. The speed reached in one second)")] 
    float _groundAcceleration = 5f;
    [SerializeField, Tooltip("Acceleration in [moveSpeed]/sec. (i.e. The speed reached in one second)")] 
    float _airAcceleration = 5f;
    [SerializeField, Tooltip("Deceleration in unit/sec�. (i.e. The speed cancelled in one second)")] 
    float _airDrag = 0.05f;
    [SerializeField, Tooltip("Speed in degrees/sec. (i.e. The angle reached in one second)")] 
    float _turnSpeed = 100f;
    [SerializeField, Tooltip("Acceleration in [turnSpeed]/sec. (i.e. The speed reached in one second)")] 
    float _turnAcceleration = 100f;
    [SerializeField] float _jumpHeight = 1f;
    [SerializeField] Vector3 _aimLocalOrigin = Vector3.up;
    [SerializeField] float _aimMaxDistance = 100f;
    [SerializeField] float _aimMaxAngle = 10f;
    [SerializeField] float _maxHealth = 100f;
    [SerializeField] float _health = 100f;
    [SerializeField, HideInInspector] int _score = 0;
    [SerializeField] Vector3 _meleeAttackOffset = Vector3.zero;
    [SerializeField] float _meleeAttackRadius = 0f;
    [SerializeField] float _groundDistance = 0.01f;
    [SerializeField] LayerMask _groundMask;
    public Transform camPivot;
    public List<Material> materials = new List<Material>();
    public List<MeshRenderer> coloredParts = new List<MeshRenderer>();
    public bool isWinner = false;
    public int index;
    [SerializeField] TMPro.TMP_Text scoringWarning;

    public delegate bool ValidateTakeDamage(Player victim, Player perp, ref float damage);
    public ValidateTakeDamage onValidateTakeDamage = null;

    [HideInInspector] public float speedMultiplier = 1f;
    [HideInInspector] public PlayerInput playerInput;
    float _moveInput = 0f;
    float _turnInput = 0f;
    bool _jumpInput = false;
    bool _wasGrounded = false, _wasOnSlope = false;
    bool _isDead = false;
    bool _tryToPickupItem = false;
    Vector3 _forwardVelocity = Vector3.zero;
    Vector3 _verticalVelocity = Vector3.zero;
    Vector3 _angularVelocity = Vector3.zero;
    CharacterController _charaControl;
    PlayerModel _playerModel;
    Toolbar _toolbar;
    StatBar _healthBar;
    PlayerConfig _config;
    Player opponent;

    private void Awake() {
        _charaControl = GetComponent<CharacterController>();
    }

    void ApplyConfig(PlayerConfig config) {
        this.name = config.name;
        //RepaintAll(config.mainMaterial);
    }

    /// <summary>
    /// Initialize the player each timpe he gets spawned.
    /// </summary>
    /// <param name="respawnPoint"></param>
    public void Spawn(Transform respawnPoint) {
        _charaControl.enabled = false;
        if(playerModel != null) Destroy(playerModel.gameObject);
        playerModel = Instantiate(GameManager.instance.playerModelPrefab, transform);
        playerModel.Initialize(false, false, this);

        scoringWarning.gameObject.SetActive(false);

        transform.position = respawnPoint.position;
        transform.rotation = respawnPoint.rotation;

        _forwardVelocity = Vector3.zero;
        _verticalVelocity = Vector3.zero;
        _angularVelocity = Vector3.zero;
        _wasOnSlope = false;
        _wasGrounded = false;
        _jumpInput = false;
        _moveInput = 0f;
        _turnInput = 0f;

        // TODO: camera initialization

        health = _maxHealth;
        isDead = false;
        _charaControl.enabled = true;
        playerModel.Show();
        UpdateModelAnimation();
    }

    private void Start() {
        _charaControl.enabled = true;
        health = _maxHealth;
        foreach (Player p in GameManager.instance.players) {
            if (p != this) opponent = p;
        }
    }
    private void FixedUpdate() {
        if(isDead) return;

        bool isOnSlope = false, isGrounded = CheckForGround(out RaycastHit hit);

        // movement (with acceleration and instant stop) and inertia (progressive)
        if(isGrounded) {
            if(_moveInput != 0) _forwardVelocity.z = Mathf.Clamp(_forwardVelocity.z + _moveInput * groundAcceleration * Time.fixedDeltaTime, -speed, speed);
            else if(_forwardVelocity.z > 0) _forwardVelocity.z = Mathf.Clamp(_forwardVelocity.z - groundAcceleration * 2 * Time.fixedDeltaTime, 0, _forwardVelocity.z);
            else if(_forwardVelocity.z < 0) _forwardVelocity.z = Mathf.Clamp(_forwardVelocity.z + groundAcceleration * 2 * Time.fixedDeltaTime, _forwardVelocity.z, 0);
        } else {
            if(_moveInput != 0) _forwardVelocity.z = Mathf.Clamp(_forwardVelocity.z + _moveInput * inAirAcceleration * Time.fixedDeltaTime, -speed, speed);
            else if(_forwardVelocity.z > 0) _forwardVelocity.z = Mathf.Clamp(_forwardVelocity.z - _airDrag * Time.fixedDeltaTime, 0, _forwardVelocity.z);
            else _forwardVelocity.z = Mathf.Clamp(_forwardVelocity.z + _airDrag * Time.fixedDeltaTime, _forwardVelocity.z, 0);
        }
        _charaControl.Move(transform.TransformDirection(_forwardVelocity * Time.fixedDeltaTime));

        // rotation (with acceleration and instant stop)
        if(_turnInput != 0) _angularVelocity.y = Mathf.Clamp(_angularVelocity.y + _turnInput * angularAcceleration * Time.fixedDeltaTime, -_turnSpeed, _turnSpeed);
        else _angularVelocity.y = 0;
        transform.Rotate(_angularVelocity * Time.fixedDeltaTime);

        // jumping and gravity
        if(isGrounded) {
            //Debug.Log("on ground");
            if (Vector3.Angle(hit.normal, Vector3.up) < _charaControl.slopeLimit) {
                // is on ground
                if (_verticalVelocity.y < 0) {
                    _verticalVelocity = Vector3.down * 2f;
                }
                if (_jumpInput) {
                    _verticalVelocity = Vector3.up * Mathf.Sqrt(_jumpHeight * -2f * Physics.gravity.y);
                }
            } else if (_verticalVelocity.y < 0) {
                // is on wall / slope
                isOnSlope = true;
                if (!_wasOnSlope) {
                    _verticalVelocity = Vector3.down * 0.5f;
                }
                Vector3 slopeVector = GetSlopeVector(hit.normal);
                //Debug.DrawRay(hit.point, slopeVector * 10, Color.blue);

                _verticalVelocity += slopeVector * Physics.gravity.magnitude * Time.fixedDeltaTime;
            }
        }

        if (!isOnSlope) _verticalVelocity += Physics.gravity * Time.fixedDeltaTime;
        _charaControl.Move(_verticalVelocity * Time.fixedDeltaTime);

        _wasOnSlope = isOnSlope;
        _wasGrounded = isGrounded;

        // Animations
        playerModel.UpdateMoveAnimation(_forwardVelocity.z / speed);

        // IK
        HandleAim();

        // Physics processus stuff
        _tryToPickupItem = false;
    }
    /// <summary>
    /// Make the player model aim at the other an offensive item is in hand.
    /// </summary>
    void HandleAim() {
        OffensiveItem offensiveItem = toolbar.selectedItem as OffensiveItem;
        if(offensiveItem == null) return; // no need to aim

        // auto target when aiming near a valid target
        Vector3 origin = playerModel.center.position;
        Vector3 castDir = playerModel.center.forward;
        Player closestPlayer = null;
        Transform target;
        // by directly raycasting the other player(s)
        foreach(var player in GameManager.instance.players) {
            // check if the player is different than us and not blocked by a wall
            if(player == this || player.isDead) continue;
            target = player.playerModel.torso;
            if(Physics.Raycast(origin, castDir, out RaycastHit hit, Vector3.Distance(origin, target.position), GameManager.instance.blockRaycastsMask)) {
                continue;
            }
            closestPlayer = player;

        }
        //if(closestPlayer != null) Debug.Log($"target found ! +> {closestPlayer}");
        playerModel.SetTarget(closestPlayer?.playerModel.torso); // set the current target
    }
    Player[] GetMeleeTargets(OffensiveItem item) {
        var targetsFound = new List<Player>();
        //Debug.Log($"{name} searches for melee targets !");
        var cols = Physics.OverlapSphere(transform.TransformPoint(_meleeAttackOffset), _meleeAttackRadius, item.targetsMask, QueryTriggerInteraction.Collide);
        //Debug.Log($"{name} found {cols.Length} targets !");
        foreach(var col in cols) {
            if(col.gameObject.TryGetPlayer(out Player target)) {
                //Debug.Log($"Found a target: {target}");
                if(target != this) {
                    targetsFound.Add(target);
                }
            }
        }
        return targetsFound.ToArray();
    }

    public void UseSelectedItem(InputAction.CallbackContext ctx) {
        if(isDead) return;
        if (ctx.phase == InputActionPhase.Performed) {
            toolbar.UseSelectedItem();
        }
    }
    public void SwitchItem(InputAction.CallbackContext ctx) {
        if(isDead) return;
        if (ctx.phase == InputActionPhase.Performed) {
            toolbar.SwitchItem();
        }
    }
    public void DestroySelectedItem(InputAction.CallbackContext ctx) {
        if(isDead) return;
        if (ctx.phase == InputActionPhase.Performed) {
            //Debug.Log($"{name} is trying to drop an item... (_tryToPickupItem = {_tryToPickupItem})");
            if (_tryToPickupItem) {
                toolbar.DestroySelectedItem();
            }
        }
    }
    public void Move(InputAction.CallbackContext ctx) {
        if(isDead) return;
        if (ctx.phase == InputActionPhase.Performed) {
            _moveInput = ctx.ReadValue<float>();
        } else if (ctx.phase == InputActionPhase.Canceled) {
            _moveInput = 0f;
        }
    }
    public void Turn(InputAction.CallbackContext ctx) {
        if(isDead) return;
        if (ctx.phase == InputActionPhase.Performed) {
            _turnInput = ctx.ReadValue<float>();
        } else if (ctx.phase == InputActionPhase.Canceled) {
            _turnInput = 0f;
        }
    }
    public void Jump(InputAction.CallbackContext ctx) {
        if(isDead) return;
        if (ctx.phase == InputActionPhase.Performed) {
            _jumpInput = true;
        } else if (ctx.phase == InputActionPhase.Canceled) {
            _jumpInput = false;
        }
    }

    public void TakeDamage(Player perp, float damage, System.Action<PlayerModel> onDieAction = null) {
        if (onValidateTakeDamage == null || onValidateTakeDamage(this, perp, ref damage)) {
            //Debug.Log($"{name} was hit for {damage} hp !");
            health -= damage;
            if(isDead) {
                onDieAction?.Invoke(playerModel);
            }
        }
    }
    public bool TryPickupItem(Item item) {
        if (toolbar.PickupItem(item)) {
            if(item.isFlag && (item as Flag).player != this) {
                opponent.scoringWarning.gameObject.SetActive(true);
                Invoke(nameof(UnactiveWarning), 5f);
            }
            UpdateItemAttachment(item);
            UpdateModelAnimation();
            return true;
        }
        return false;
    }
    public bool RemoveItemFromToolbar(Item item) {
        return toolbar.RemoveItem(item);
    }

    public void OnItemSwitched(Item previousItem) {
        UpdateAllItemAttachments();
        UpdateModelAnimation();
    }
    public void OnItemRemoved(Item removedItem) {
        if (removedItem == toolbar.selectedItem) {
            ResetModelAnimation();
        }
    }
    public void PlayAnimationTrigger(PlayerAnim anim) {
        if(playerModel.PlayAnimationTrigger(anim)) {
            ActiveItem item;
            if((item = toolbar.selectedItem as ActiveItem) != null) {
                item.isInAnimation = true;
            }
        }
    }
    public void DoItemAttack() {
        //Debug.Log($"{name} attack with {toolbar.selectedItem} !");
        OffensiveItem item;
        if((item = toolbar.selectedItem as OffensiveItem) != null) {
            item.DoDamage(GetMeleeTargets(item));
        }
    }
    public void OnEndUseAnimation() {
        ActiveItem item;
        if((item = toolbar.selectedItem as ActiveItem) != null) {
            item.OnEndUseAnimation();
        }
    }

    void Die() {
        if (isDead) return;
        //Debug.Log($"{name} died ! :O");
        isDead = true;
        ClearItems();
        playerModel.ToggleRagdoll(true);
        GameManager.instance.OnPlayerDeath(this);
    }

    void ClearItems() {
        Item[] items = toolbar.GetItems();
        foreach(var item in items) {
            if(!item.isFlag) item.Break();
            else item.Drop();
        }
    }

    /// <summary>
    /// Update the animation and layer of model's animator based on the current item's configuration.
    /// </summary>
    void UpdateModelAnimation() {
        if(toolbar.selectedItem == null) {
            ResetModelAnimation();
        } else {
            Item currentItem = toolbar.selectedItem;
            bool useRightHand = currentItem.activeAttachmentPart == BodyPart.RightHand || currentItem.activeHoldingMode == Item.HoldingMode.Shotgun;
            bool useLeftHand = currentItem.activeAttachmentPart == BodyPart.LeftHand || currentItem.activeHoldingMode == Item.HoldingMode.Shotgun;
            bool shouldBeHolding = currentItem.activeHoldingMode != Item.HoldingMode.None;
            playerModel.FadeRightArmLayer(0.2f, shouldBeHolding && useRightHand);
            playerModel.FadeLeftArmLayer(0.2f, shouldBeHolding && useLeftHand);
            var offensiveItem = currentItem as OffensiveItem;
            if(offensiveItem != null && (currentItem.activeHoldingMode == Item.HoldingMode.Gun || currentItem.activeHoldingMode == Item.HoldingMode.Shotgun)) {
                playerModel.SetAim(offensiveItem, _aimMaxDistance, _aimMaxAngle);
            } else {
                playerModel.StopAiming();
            }
        }
    }
    void ResetModelAnimation() {
        playerModel.FadeRightArmLayer(0.2f, false);
        playerModel.FadeLeftArmLayer(0.2f, false);
        playerModel.StopAiming();
    }
    void UpdateAllItemAttachments() {
        var items = toolbar.GetItems();
        foreach(var item in items) {
            UpdateItemAttachment(item);
        }
    }
    /// <summary>
    /// Update the position and rotation and scale of the item based on wher it's supposed to be. (ex: item in hand => handheld attachment).
    /// </summary>
    /// <param name="item"></param>
    void UpdateItemAttachment(Item item) {
        if(item == toolbar.selectedItem) {
            item.AttachTo(playerModel.GetBodyPartTransform(item.activeAttachmentPart), Item.AttachmentMode.HandHeld);
        } else {
            item.AttachTo(playerModel.GetBodyPartTransform(item.inactiveAttachmentPart), Item.AttachmentMode.Holstered);
        }
        item.ResetTransform();
    }

    Vector3 GetSlopeVector(Vector3 normal) {
        return Vector3.Cross(Vector3.Cross(normal, Vector3.down), normal).normalized;
    }
    bool CheckForGround() => CheckForGround(out RaycastHit hit);
    bool CheckForGround(out RaycastHit hit) {
        Vector3 center = transform.position + _charaControl.center + Vector3.down * (_charaControl.height * 0.5f - _charaControl.radius);
        Vector3 dir = Vector3.down * _groundDistance;
        if (Physics.SphereCast(center, _charaControl.radius, dir.normalized, out hit, dir.magnitude, _groundMask)) {
            return true;
        }
        return false;
    }

    public override string ToString() {
        return $"(Player => name: {name})";
    }

    private void OnTriggerStay(Collider other) {
        //Debug.Log($"{this} has triggered {other.name}");
        if(isDead) return;
        if (other.gameObject.TryGetItem(out Item item)) {
            item.Trigger(this, true);
            Flag flag = item as Flag;
            //Debug.Log($"{item} : flag = {flag} : this player flag = {flag.player == this} : not in base = {!flag.isInBase}");
            if (item.canBePickedUp && (flag != null && flag.player == this ? !flag.isInBase : true)) {
                //Debug.Log("item try pickup");
                if(flag != null && flag.player != this) flag.isInBase = false;
                _tryToPickupItem = true;
                TryPickupItem(item);
            }
        }
    }
    private void OnTriggerEnter(Collider other) {
        if(isDead) return;
        if(other.tag.StartsWith("Base")) {
            Flag[] flags = toolbar.GetItems().Where(item => item.isFlag).Cast<Flag>().ToArray();
            foreach(var flag in flags) {
                if(other.CompareTag("Base" + (flag.player.index + 1))) {
                    flag.Break();
                } else {
                    score++;
                    flag.Break();
                }
            }
        }
    }
    private void UnactiveWarning() {
        opponent.scoringWarning.gameObject.SetActive(false);
    }

#if UNITY_EDITOR

    private void OnDrawGizmosSelected() {
        Color gizmoColor = Gizmos.color;
        Gizmos.color = Color.red;

        Gizmos.DrawWireSphere(transform.TransformPoint(_meleeAttackOffset), _meleeAttackRadius);

        Gizmos.color = gizmoColor;
    }

#endif
}