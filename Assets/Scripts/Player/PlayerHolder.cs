using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHolder : MonoBehaviour
{
    public Canvas canvas;
    public Camera cam;
    public Cinemachine.CinemachineFreeLook freeLook;
    public Player player;

    public void Initialize(int i) {
        freeLook.LookAt = player.camPivot;
        freeLook.Follow = player.transform;

        int layer = LayerMask.NameToLayer("Cam " + (i + 1));
        cam.cullingMask |= 1 << layer;
        freeLook.gameObject.layer = layer;

        //playerCanvas.targetDisplay = playerCamera.targetDisplay = i;

        player.toolbar = canvas.GetComponentInChildren<Toolbar>();
        player.healthBar = canvas.GetComponentInChildren<StatBar>();

        var rt = canvas.transform.GetChild(0).GetComponent<RectTransform>();
        rt.anchorMin = new Vector2(i * 0.5f,0);
        rt.anchorMax = new Vector2(0.5f + i *0.5f,1);
    }
}
