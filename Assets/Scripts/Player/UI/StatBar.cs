using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StatBar : MonoBehaviour {
    public Color backgroundColor {
        get => _backgroundColor;
        set {
            _backgroundColor = value;
        }
    }
    public Color fillColor {
        get => _fillColor;
        set {
            _fillColor = value;
        }
    }
    public float alpha {
        get => _alpha;
        set {
            _alpha = value;
        }
    }
    public float statValue {
        get => _slider.value;
        set {
            _slider.value = value;
            _textDisplay.text = _slider.value.ToString("0") + " / " + _slider.maxValue.ToString("0");
        }
    }

    [Header("Slider")]
    [SerializeField] Slider _slider = null;
    [SerializeField, Range(0, 1f)] float _alpha;
    [SerializeField] Color _backgroundColor;
    [SerializeField] Color _fillColor;
    [Header("Specifics")]
    [SerializeField] TextMeshProUGUI _textDisplay;

    public void Setup(float minValue, float maxValue) {
        if(_slider != null) {
            _slider.minValue = minValue;
            _slider.maxValue = maxValue;
        }
    }

    public void UpdateSliderColors() {
        if(_slider == null) return;

        if(_slider.fillRect.TryGetComponent(out Image fillImage)) {
            fillImage.color = new Color(fillColor.r, fillColor.g, fillColor.b, alpha);
        }
        if(_slider.transform.GetChild(0).TryGetComponent(out Image backgroundImage)) {
            backgroundImage.color = new Color(backgroundColor.r, backgroundColor.g, backgroundColor.b, alpha);
        }
    }
}