using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolbarSlot : MonoBehaviour {
    public Item item {
        get => _item;
        set {
            _item = value;
            _img.sprite = value != null ? _item.sprite : null;
            _img.color = value != null ? (_item is Flag ? (_item as Flag).color : Color.white) : Color.clear;
        }
    }

    [SerializeField] Image _img;

    Item _item;

    public void Clear() {
        item = null;
    }
}