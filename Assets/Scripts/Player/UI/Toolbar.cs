using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toolbar : MonoBehaviour {
    public bool isFull { get => _nbEmptySlots == 0; }
    public Item selectedItem {
        get => slots[_selectedIndex]?.item;
    }
    public Player owner {
        get => _player;
        set {
            _player = value;
        }
    }

    [SerializeField] ToolbarSlot _tbItemPrefab;
    ToolbarSlot[] slots;
    int _selectedIndex = 0, _nbEmptySlots;
    Player _player;

    private void Awake() {
        // get slots
        slots = GetComponentsInChildren<ToolbarSlot>();
        for(int i = 0; i < slots.Length; i++) {
            slots[i].Clear();
        }
        _nbEmptySlots = slots.Length;
        //Debug.Log($"Number of slots ({slots.Length}) in which ({_nbEmptySlots}) are empty. Is the toolbar full ? {isFull}");
    }

    public void UseSelectedItem() {
        //Debug.Log($"{owner.name} is trying to use {selectedItem}.");
        ActiveItem aItem = selectedItem as ActiveItem;
        if(aItem != null) {
            aItem.Use();
        }
    }
    public void DestroySelectedItem() {
        //Debug.Log($"{owner.name} is trying to destroy {selectedItem}");
        if(selectedItem != null && selectedItem.canBeDropped && !selectedItem.isFlag) {
            selectedItem.Break();
        }
    }
    public void SwitchItem() {
        Item previousItem = selectedItem;
        if(selectedItem != null && !selectedItem.canBeSwitched) return;

        _selectedIndex = (_selectedIndex + 1) % slots.Length;
        //Debug.Log($"{owner.name} is switching to {selectedItem}. (index: {_selectedIndex})");
        previousItem?.OnSwitchFocus(false);
        selectedItem?.OnSwitchFocus(true);
        owner.OnItemSwitched(previousItem);
    }

    public bool PickupItem(Item item) {
        //Debug.Log($"Toolbar is trying to store the item... (isFull ? {isFull})");
        if(item.canBeStored) {
            if(!isFull) {
                item.Pickup(owner);
                if(item.canBeStored) {
                    // store item
                    ToolbarSlot slot = GetFirstEmptySlot();
                    slot.item = item;
                    _nbEmptySlots--;
                }
            } else {
                return false;
            }
        } else {
            item.Pickup(owner);
        }
        return true;
    }

    public bool RemoveItem(Item item) {
        if(!item.canBeStored) return true;
        ToolbarSlot slot = GetSlot(item);
        if(slot != null) {
            owner.OnItemRemoved(item);
            slot.Clear(); // remove from slot
            _nbEmptySlots++;
            return true;
        }
        return false;
    }

    public Item GetRandomItem() {
        var items = GetItems();
        if(items.Length == 0) return null;
        else if(items.Length == 1) return items[0];

        return items[Random.Range(0, items.Length)];
    }
    public Item[] GetItems() {
        var itemList = new List<Item>();
        foreach(var slot in slots) {
            if(slot.item != null) itemList.Add(slot.item);
        }
        return itemList.ToArray();
    }
    ToolbarSlot GetSlot(Item item) {
        if(item == null) return null;
        return slots.FirstOrDefault(slot => slot.item == item);
    }
    ToolbarSlot GetFirstEmptySlot() {
        foreach(var slot in slots) {
            if(slot.item == null) return slot;
        }
        return null;
    }
}