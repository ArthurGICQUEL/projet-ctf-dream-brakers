using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(StatBar))]
public class StatBarInspector : Editor {

    private void OnEnable() {
        ((StatBar)target).UpdateSliderColors();
    }

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        StatBar statBar = (StatBar)target;
        statBar.UpdateSliderColors();
    }
}