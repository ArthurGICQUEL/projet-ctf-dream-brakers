using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class UIRebindingOverlay : MonoBehaviour {
    [SerializeField] Text instructionInfo, currentKeyInfo, cancelInfo;

    public void SetCurrentKeyName(string keyName) {
        currentKeyInfo.text = $"- Current is <{keyName.ToUpper()}> -";
    }
    public void SetCancelKeyName(string keyName) {
        cancelInfo.text = $"Press <{keyName.ToUpper()}> to cancel";
    }
    public void Show(InputActionReference actionRef, int bindingIndex) {
        gameObject.SetActive(true);
        SetCurrentKeyName(
            actionRef.action.GetBindingDisplayString(bindingIndex, InputBinding.DisplayStringOptions.DontUseShortDisplayNames));
        //SetCancelKeyName(
        //    DataManager.instance.GetCancelAction()
        //    .GetBindingDisplayString(0, InputBinding.DisplayStringOptions.DontUseShortDisplayNames));
    }
    public void Hide() {
        gameObject.SetActive(false);
    }
}