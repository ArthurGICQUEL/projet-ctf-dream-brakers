//
// This is a modified script from the rebinding sample of the 1.0.2 preview of the InputSystem package.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;
using UnityEngine.UI;

public class UIRebinder : MonoBehaviour {
    private static List<UIRebinder> s_RebindActionUIs;

    public bool isRebindable {
        get => m_Rebindable;
        set {
            m_Rebindable = value;
            UpdateRebindable();
        }
    }

    /// <summary>
    /// Reference to the action that is to be rebound.
    /// </summary>
    public InputActionReference actionReference {
        get => m_Action;
        set {
            m_Action = value;
            UpdateActionLabel();
            UpdateBindingDisplay();
        }
    }

    /// <summary>
    /// ID (in string form) of the binding that is to be rebound on the action.
    /// </summary>
    /// <seealso cref="InputBinding.id"/>
    public string bindingId {
        get => m_BindingId;
        set {
            m_BindingId = value;
            UpdateBindingDisplay();
        }
    }

    public InputBinding.DisplayStringOptions displayStringOptions {
        get => m_DisplayStringOptions;
        set {
            m_DisplayStringOptions = value;
            UpdateBindingDisplay();
        }
    }

    /// <summary>
    /// Text component that receives the name of the action. Optional.
    /// </summary>
    public Text actionLabel {
        get => m_ActionLabel;
        set {
            m_ActionLabel = value;
            UpdateActionLabel();
        }
    }

    /// <summary>
    /// Text component that receives the display string of the binding for keyboard. Can be <c>null</c> in which
    /// case the component entirely relies on <see cref="updateBindingUIEvent"/>.
    /// </summary>
    public Text bindingText {
        get => m_BindingText;
        set {
            m_BindingText = value;
            UpdateBindingDisplay();
        }
    }

    /// <summary>
    /// Text component that receives the display image of the binding for gamepad. Can be <c>null</c> in which
    /// case the component entirely relies on <see cref="updateBindingUIEvent"/>.
    /// </summary>
    public Image bindingIcon {
        get => m_BindingIcon;
        set {
            m_BindingIcon = value;
            UpdateBindingDisplay();
        }
    }

    public Button rebindButton {
        get => m_RebindButton;
        set {
            m_RebindButton = value;
            UpdateRebindable();
        }
    }

    public Button resetButton {
        get => m_ResetButton;
        set {
            m_ResetButton = value;
            UpdateRebindable();
        }
    }

    /// <summary>
    /// Event that is triggered every time the UI updates to reflect the current binding.
    /// This can be used to tie custom visualizations to bindings.
    /// </summary>
    public UpdateBindingUIEvent updateBindingUIEvent {
        get {
            if(m_UpdateBindingUIEvent == null)
                m_UpdateBindingUIEvent = new UpdateBindingUIEvent();
            return m_UpdateBindingUIEvent;
        }
    }

    /// <summary>
    /// Event that is triggered when an interactive rebind is started on the action.
    /// </summary>
    public InteractiveRebindEvent startRebindEvent {
        get {
            if(m_RebindStartEvent == null)
                m_RebindStartEvent = new InteractiveRebindEvent();
            return m_RebindStartEvent;
        }
    }

    /// <summary>
    /// Event that is triggered when an interactive rebind has been completed or canceled.
    /// </summary>
    public InteractiveRebindEvent stopRebindEvent {
        get {
            if(m_RebindStopEvent == null)
                m_RebindStopEvent = new InteractiveRebindEvent();
            return m_RebindStopEvent;
        }
    }

    /// <summary>
    /// When an interactive rebind is in progress, this is the rebind operation controller.
    /// Otherwise, it is <c>null</c>.
    /// </summary>
    public InputActionRebindingExtensions.RebindingOperation ongoingRebind => m_RebindOperation;

    [Tooltip("Controls if the Rebind and Reset buttons will be interactable.")]
    [SerializeField]
    private bool m_Rebindable = true;

    [Tooltip("Reference to action that is to be rebound from the UI.")]
    [SerializeField]
    private InputActionReference m_Action;

    [SerializeField]
    private string m_BindingId;

    [SerializeField]
    private InputBinding.DisplayStringOptions m_DisplayStringOptions;

    [Tooltip("Text label that will receive the name of the action. Optional. Set to None to have the "
        + "rebind UI not show a label for the action.")]
    [SerializeField]
    private Text m_ActionLabel;

    [Tooltip("Text label that will receive the current, formatted binding string. (for keyboard)")]
    [SerializeField]
    private Text m_BindingText;

    [Tooltip("Image that will receive the corresponding icon. (for gamepad)")]
    [SerializeField]
    private Image m_BindingIcon;

    [SerializeField]
    private Button m_RebindButton;

    [SerializeField]
    private Button m_ResetButton;

    [Tooltip("Event that is triggered when the way the binding is display should be updated. This allows displaying "
        + "bindings in custom ways, e.g. using images instead of text.")]
    [SerializeField]
    private UpdateBindingUIEvent m_UpdateBindingUIEvent;

    [Tooltip("Event that is triggered when an interactive rebind is being initiated. This can be used, for example, "
        + "to implement custom UI behavior while a rebind is in progress. It can also be used to further "
        + "customize the rebind.")]
    [SerializeField]
    private InteractiveRebindEvent m_RebindStartEvent;

    [Tooltip("Event that is triggered when an interactive rebind is complete or has been aborted.")]
    [SerializeField]
    private InteractiveRebindEvent m_RebindStopEvent;

    private InputActionRebindingExtensions.RebindingOperation m_RebindOperation;

    [HideInInspector] public RebindingMenu menu;

    private void Awake() {
        menu = GetComponentInParent<RebindingMenu>();
    }

    /// <summary>
    /// Return the action and binding index for the binding that is targeted by the component
    /// according to
    /// </summary>
    /// <param name="action"></param>
    /// <param name="bindingIndex"></param>
    /// <returns></returns>
    public bool ResolveActionAndBinding(out InputAction action, out int bindingIndex) {
        bindingIndex = -1;

        action = m_Action?.action;
        if(action == null)
            return false;

        if(string.IsNullOrEmpty(m_BindingId))
            return false;

        // Look up binding index.
        var bindingId = new Guid(m_BindingId);
        bindingIndex = action.bindings.IndexOf(x => x.id == bindingId);
        if(bindingIndex == -1) {
            Debug.LogError($"Cannot find binding with ID '{bindingId}' on '{action}'", this);
            return false;
        }

        return true;
    }

    /// <summary>
    /// Trigger a refresh of the currently displayed binding.
    /// </summary>
    public void UpdateBindingDisplay() {
        var displayString = string.Empty;
        var deviceLayoutName = default(string);
        var controlPath = default(string);

        // Get display string from action.
        var action = m_Action?.action;
        if(action != null) {
            var bindingIndex = action.bindings.IndexOf(x => x.id.ToString() == m_BindingId);
            if(bindingIndex != -1)
                displayString = action.GetBindingDisplayString(bindingIndex, out deviceLayoutName, out controlPath, displayStringOptions);
        }

        // Set on label (if any).
        if(m_BindingText != null)
            m_BindingText.text = displayString.ToUpper();

        // Give listeners a chance to configure UI in response.
        m_UpdateBindingUIEvent?.Invoke(this, displayString, deviceLayoutName, controlPath);
    }

    private void UpdateActionLabel() {
        if(m_ActionLabel != null) {
            var action = m_Action?.action;
            string label = string.Empty;
            if(action != null) {
                var bindingId = new Guid(m_BindingId);
                if(action.bindings.Any(x => x.id == bindingId)) {
                    label = action.name;
                    var binding = action.bindings.First(x => x.id == bindingId);
                    if(binding.isPartOfComposite) {
                        label += $" ({binding.name})"; 
                    }
                    label += " :";
                }
            }
            m_ActionLabel.text = label;
        }
    }

    private void UpdateRebindable() {
        if(rebindButton != null) rebindButton.interactable = isRebindable;
        if(resetButton != null) resetButton.interactable = isRebindable;
    }

    /// <summary>
    /// Remove currently applied binding overrides.
    /// </summary>
    public void ResetToDefault() {
        if(!ResolveActionAndBinding(out var action, out var bindingIndex))
            return;

        if(action.bindings[bindingIndex].isComposite) {
            // It's a composite. Remove overrides from part bindings.
            for(var i = bindingIndex + 1; i < action.bindings.Count && action.bindings[i].isPartOfComposite; ++i)
                action.RemoveBindingOverride(i);
        } else {
            action.RemoveBindingOverride(bindingIndex);
        }
        UpdateBindingDisplay();
    }

    /// <summary>
    /// Initiate an interactive rebind that lets the player actuate a control to choose a new binding
    /// for the action.
    /// </summary>
    public void StartInteractiveRebind() {
        if(!ResolveActionAndBinding(out var action, out var bindingIndex))
            return;

        // If the binding is a composite, we need to rebind each part in turn.
        if(action.bindings[bindingIndex].isComposite) {
            var firstPartIndex = bindingIndex + 1;
            if(firstPartIndex < action.bindings.Count && action.bindings[firstPartIndex].isPartOfComposite)
                PerformInteractiveRebind(action, firstPartIndex, allCompositeParts: true);
        } else {
            PerformInteractiveRebind(action, bindingIndex);
        }
    }

    private void PerformInteractiveRebind(InputAction action, int bindingIndex, bool allCompositeParts = false) {
        m_RebindOperation?.Cancel(); // Will null out m_RebindOperation.

        void CleanUp() {
            m_RebindOperation?.Dispose();
            m_RebindOperation = null;
        }

        // Configure the rebind.
        m_RebindOperation = action.PerformInteractiveRebinding(bindingIndex)
            .OnCancel(
                operation => {
                    menu.rebindingOverlay.Hide();
                    m_RebindStopEvent?.Invoke(this, operation);
                    UpdateBindingDisplay();
                    CleanUp();
                })
            .OnComplete(
                operation => {
                    menu.rebindingOverlay.Hide();
                    m_RebindStopEvent?.Invoke(this, operation);
                    UpdateBindingDisplay();
                    CleanUp();

                    // If there's more composite parts we should bind, initiate a rebind
                    // for the next part.
                    if(allCompositeParts) {
                        var nextBindingIndex = bindingIndex + 1;
                        if(nextBindingIndex < action.bindings.Count && action.bindings[nextBindingIndex].isPartOfComposite)
                            PerformInteractiveRebind(action, nextBindingIndex, true);
                    }
                });

        // If it's a part binding, show the name of the part in the UI.
        var partName = default(string);
        if(action.bindings[bindingIndex].isPartOfComposite)
            partName = $"Binding '{action.bindings[bindingIndex].name}'. ";

        // Bring up rebind overlay, if we have one.
        var text = !string.IsNullOrEmpty(m_RebindOperation.expectedControlType)
            ? $"{partName}Waiting for {m_RebindOperation.expectedControlType} input..."
            : $"{partName}Waiting for input...";
        menu.rebindingOverlay.Show(actionReference, bindingIndex);

        // If we have no rebind overlay and no callback but we have a binding text label,
        // temporarily set the binding text label to "<Waiting>".
        if(menu.rebindingOverlay == null && m_RebindStartEvent == null && m_BindingText != null)
            m_BindingText.text = "<Waiting...>";

        // Give listeners a chance to act on the rebind starting.
        m_RebindStartEvent?.Invoke(this, m_RebindOperation);

        m_RebindOperation.Start();
    }

    protected void OnEnable() {
        if(s_RebindActionUIs == null)
            s_RebindActionUIs = new List<UIRebinder>();
        s_RebindActionUIs.Add(this);
        if(s_RebindActionUIs.Count == 1)
            InputSystem.onActionChange += OnActionChange;
    }

    protected void OnDisable() {
        m_RebindOperation?.Dispose();
        m_RebindOperation = null;

        s_RebindActionUIs.Remove(this);
        if(s_RebindActionUIs.Count == 0) {
            s_RebindActionUIs = null;
            InputSystem.onActionChange -= OnActionChange;
        }
    }

    // When the action system re-resolves bindings, we want to update our UI in response. While this will
    // also trigger from changes we made ourselves, it ensures that we react to changes made elsewhere. If
    // the user changes keyboard layout, for example, we will get a BoundControlsChanged notification and
    // will update our UI to reflect the current keyboard layout.
    private static void OnActionChange(object obj, InputActionChange change) {
        if(change != InputActionChange.BoundControlsChanged)
            return;

        var action = obj as InputAction;
        var actionMap = action?.actionMap ?? obj as InputActionMap;
        var actionAsset = actionMap?.asset ?? obj as InputActionAsset;

        for(var i = 0; i < s_RebindActionUIs.Count; ++i) {
            var component = s_RebindActionUIs[i];
            var referencedAction = component.actionReference?.action;
            if(referencedAction == null)
                continue;

            if(referencedAction == action ||
                referencedAction.actionMap == actionMap ||
                referencedAction.actionMap?.asset == actionAsset)
                component.UpdateBindingDisplay();
        }
    }

    // We want the label for the action name to update in edit mode, too, so
    // we kick that off from here.
#if UNITY_EDITOR
    protected void OnValidate() {
        UpdateActionLabel();
        UpdateBindingDisplay();
        UpdateRebindable();
    }

#endif

    [Serializable]
    public class UpdateBindingUIEvent : UnityEvent<UIRebinder, string, string, string> {
    }

    [Serializable]
    public class InteractiveRebindEvent : UnityEvent<UIRebinder, InputActionRebindingExtensions.RebindingOperation> {
    }
}
