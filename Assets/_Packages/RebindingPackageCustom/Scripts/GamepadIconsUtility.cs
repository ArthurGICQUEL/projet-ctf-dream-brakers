//
// This is a modified script from the rebinding sample of the 1.0.2 preview of the InputSystem package.
//

using System;
using UnityEngine.InputSystem;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This is an example for how to override the default display behavior of bindings. The component
/// hooks into <see cref="UIRebinder.updateBindingUIEvent"/> which is triggered when UI display
/// of a binding should be refreshed. It then checks whether we have an icon for the current binding
/// and if so, replaces the default text display with an icon.
/// </summary>
public class GamepadIconsUtility : MonoBehaviour {
    public GamepadIcons xbox;
    public GamepadIcons ps4;

    protected void OnEnable() {
        HookGamepadIcons();
    }

    public void HookGamepadIcons() {
        // Hook into all updateBindingUIEvents on all UIRebinder components in our hierarchy.
        var rebindUIComponents = FindObjectsOfType<UIRebinder>(true);
        foreach(var component in rebindUIComponents) {
            component.updateBindingUIEvent.AddListener(OnUpdateBindingDisplay);
            component.UpdateBindingDisplay();
        }
    }

    protected void OnUpdateBindingDisplay(UIRebinder component, string bindingDisplayString, string deviceLayoutName, string controlPath) {
        if(string.IsNullOrEmpty(deviceLayoutName) || string.IsNullOrEmpty(controlPath))
            return;
        //Debug.Log(controlPath);
        var icon = default(Sprite);
        if(InputSystem.IsFirstLayoutBasedOnSecond(deviceLayoutName, "DualShockGamepad"))
            icon = ps4.GetSprite(controlPath);
        else if(InputSystem.IsFirstLayoutBasedOnSecond(deviceLayoutName, "Gamepad"))
            icon = xbox.GetSprite(controlPath);

        var textComponent = component.bindingText;
        var imageComponent = component.bindingIcon;

        if(icon != null) {
            textComponent.gameObject.SetActive(false);
            imageComponent.color = Color.white;
            imageComponent.sprite = icon;
        } else {
            textComponent.gameObject.SetActive(true);
            imageComponent.color = Color.clear;
        }
    }

    [Serializable]
    public struct GamepadIcons {
        public Sprite buttonSouth;
        public Sprite buttonNorth;
        public Sprite buttonEast;
        public Sprite buttonWest;
        public Sprite startButton;
        public Sprite selectButton;
        public Sprite leftTrigger;
        public Sprite rightTrigger;
        public Sprite leftShoulder;
        public Sprite rightShoulder;
        public Sprite dpad;
        public Sprite dpadUp;
        public Sprite dpadDown;
        public Sprite dpadLeft;
        public Sprite dpadRight;
        public Sprite leftStick;
        public Sprite leftStickHorizontal;
        public Sprite leftStickVertical;
        public Sprite leftStickUp;
        public Sprite leftStickDown;
        public Sprite leftStickRight;
        public Sprite leftStickLeft;
        public Sprite leftStickPress;
        public Sprite rightStick;
        public Sprite rightStickHorizontal;
        public Sprite rightStickVertical;
        public Sprite rightStickUp;
        public Sprite rightStickDown;
        public Sprite rightStickRight;
        public Sprite rightStickLeft;
        public Sprite rightStickPress;

        public Sprite GetSprite(string controlPath) {
            // From the input system, we get the path of the control on device. So we can just
            // map from that to the sprites we have for gamepads.
            switch(controlPath) {
                case "buttonSouth": return buttonSouth;
                case "buttonNorth": return buttonNorth;
                case "buttonEast": return buttonEast;
                case "buttonWest": return buttonWest;
                case "start": return startButton;
                case "select": return selectButton;
                case "leftTrigger": return leftTrigger;
                case "rightTrigger": return rightTrigger;
                case "leftShoulder": return leftShoulder;
                case "rightShoulder": return rightShoulder;
                case "dpad": return dpad;
                case "dpad/up": return dpadUp;
                case "dpad/down": return dpadDown;
                case "dpad/left": return dpadLeft;
                case "dpad/right": return dpadRight;
                case "leftStick": return leftStick;
                case "leftStick/x": return leftStickHorizontal;
                case "leftStick/y": return leftStickVertical;
                case "leftStick/up": return leftStickUp;
                case "leftStick/down": return leftStickDown;
                case "leftStick/right": return leftStickRight;
                case "leftStick/left": return leftStickLeft;
                case "rightStick": return rightStick;
                case "rightStick/x": return rightStickHorizontal;
                case "rightStick/y": return rightStickVertical;
                case "rightStick/up": return rightStickUp;
                case "rightStick/down": return rightStickDown;
                case "rightStick/right": return rightStickRight;
                case "rightStick/left": return rightStickLeft;
                case "leftStickPress": return leftStickPress;
                case "rightStickPress": return rightStickPress;
            }
            return null;
        }
    }
}