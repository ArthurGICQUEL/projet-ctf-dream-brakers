// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/simpleTerrain"
{
    Properties
    {
        _NoiseTex("Noise Texture", 2D) = "white" {}
        _MainTex("Albedo (RGB)", 2D) = "white" {}
        _GrassColor("Grass Color", Color) = (1,1,1,1)
        _CliffColor("Cliff Color", Color) = (1,1,1,1)
        _CliffEdgeStrength("Cliff Edge Strength", Range(1,100)) = 1.0
        _CliffAngleOffset("Cliff Angle Offset", Range(-90,90)) = 0.0
        _CliffNoiseScale("Cliff Noise Scale", Range(0,1000)) = 1
        _CliffNoiseOffset("Cliff Noise Offset", float) = 0.0
        _SandColor("Sand Color", Color) = (1, 1, 1, 1)
        _SandThreshold("Sand Threshold", float) = 0.0
        _SandEdgeMargin("Sand Edge Margin", Range(0,100)) = 0.0
        _SandNoiseScale("Sand Noise Scale", Range(0,1000)) = 1
        _SandNoiseOffset("Sand Noise Offset", float) = 0.0
        _Glossiness("Smoothness", Range(0, 1)) = 0.5
        _Metallic("Metallic", Range(0, 1)) = 0.0
        _TestDirection("Testing Direction", Vector) = (0,1,0)
        _TestAngle("Testing Angle", float) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM

        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows //vertex:myvert

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _NoiseTex;
        fixed4 _GrassColor;
        fixed4 _CliffColor;
        float _CliffEdgeStrength;
        float _CliffAngleOffset;
        float _CliffNoiseScale;
        float _CliffNoiseOffset;
        fixed4 _SandColor;
        float _SandThreshold;
        float _SandEdgeMargin;
        float _SandNoiseScale;
        float _SandNoiseOffset;

        float4 _TestDirection;

        struct Input
        {
            float2 uv_MainTex;
            float3 worldNormal;
            float3 worldPos;
            INTERNAL_DATA
        };

        half _Glossiness;
        half _Metallic;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
        // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        /*void myvert(inout appdata_full v, out Input data) {
            UNITY_INITIALIZE_OUTPUT(Input, data);
            data.worldNormal = mul((float3x3)unity_ObjectToWorld, v.normal);
        }*/

        inline float angleBetween(float3 v, float3 original) {
            //return acos(dot(v, original) / (length(v) * length(original)));
            return dot(normalize(v), original);
        }
        inline float sigmoid(float val) {
            return 1.0 / (1.0 + exp(-val));
        }
        inline float colorStrength(float inVal, float strength) {
            return sigmoid((strength + 0.5) * 5 * inVal);
        }
        inline float clamp(float val, float min, float max) {
            return val < min ? min : (val > max ? max : val);
        }
        inline float cutout(float val, float threshold) {
            return (val >= threshold ? 1 : 0);
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex);


            //float noiseSample = colorStrength(tex2Dlod(_NoiseTex, float4(IN.worldPos.xz * _NoiseScale + _NoiseOffset, 0, 0)) * 2 - 1, _NoiseSteepness);
            float cliffNoise = tex2Dlod(_NoiseTex, float4(IN.worldPos.xz / _CliffNoiseScale + _CliffNoiseOffset, 0, 0));
            float cliffRatio = colorStrength(-(angleBetween(IN.worldNormal, float3(0,1,0)) * 2 - 1 + _CliffAngleOffset / 90), _CliffEdgeStrength);
            cliffRatio = cutout(cliffNoise, cliffRatio);

            float sandNoise = tex2Dlod(_NoiseTex, float4(IN.worldPos.xz / _SandNoiseScale + _SandNoiseOffset, 0, 0));
            float sandRatio = colorStrength((IN.worldPos.y - _SandThreshold) / (_SandEdgeMargin / 2), 1);
            sandRatio = cutout(sandNoise, sandRatio);
            
            //sandRatio *= noiseSample;

            //c = c * lerp(_SandColor, lerp(_GrassColor, _CliffColor, cliffRatio), sandRatio);
            c *= lerp(lerp(_CliffColor, _GrassColor, cliffRatio), _SandColor, sandRatio);

            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }

        ENDCG
    }
    FallBack "Diffuse"
}
